/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Centralized control for stream accumulators.
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module StreamAccumulatorControl #(W)
(
	input wire iClock,
	input wire iReset,
	output logic oReady
);

// -- The total width necessary for maximum accuracy.
localparam TW = 2 * W - 2;

// -- Tick counter.
logic [TW:1] Tick = '0;
wire [TW:1] NextTick, LastTick;

assign { LastTick, NextTick } = Tick + 1;

always @(posedge iClock)
	Tick <= iReset ? '0 : NextTick;
	
// -- Ready signal.
assign oReady = LastTick & ~iReset;

endmodule
