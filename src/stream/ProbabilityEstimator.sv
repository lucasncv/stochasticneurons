/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Estimates the probability of a stochastic bitstream.
* Author: Lucas Neves Carvalho (100% TESTED!)
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module ProbabilityEstimator(RESET, CLOCK, STREAM, PROBABILITY, READY);

// -- The binary probability width.
parameter WIDTH = 4;

// -- The total width necessary for maximum accuracy.
localparam TOTALW = 2 * WIDTH - 2;

// -- The inputs.
input logic RESET;
input logic CLOCK;
input logic STREAM;

// -- The outputs.
output logic [WIDTH:1] PROBABILITY;
output logic READY = 0;

// -- Tick counter.
logic [TOTALW:1] Tick = 0;
wire [TOTALW:1] NextTick;
wire LastTick;

assign { LastTick, NextTick } = Tick + 1;

always @(posedge CLOCK)
	Tick <= RESET ? 'b0 : NextTick;
	
// -- Ready signal.
always @(posedge CLOCK)
	READY <= LastTick & ~RESET;
    
// -- Accumulates bits from the bit-stream.
bit [TOTALW:1] Accumulator;

always @(posedge CLOCK)
	Accumulator <= RESET || LastTick ? 'b0 : Accumulator + STREAM;
    
// -- Outputs the probability after every cycle.
always @(posedge CLOCK)
	if (RESET)
		PROBABILITY <= 'b0;
	else if (LastTick)
		PROBABILITY <= Accumulator[1 + TOTALW - WIDTH +: WIDTH];

endmodule
