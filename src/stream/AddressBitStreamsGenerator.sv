/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Address Bit-Streams Generator. Used by 8-bit LUT Bit-Stream Generators.
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module AddressBitStreamsGenerator(CLOCK, CARRIERS, STREAMS);

// -- The inputs.
input wire CLOCK;
input bit [16:0] CARRIERS;

// -- The outputs.
output bit [2:0] STREAMS;

// -- Interlaced carrier streams.
//wire [3:0] Carriers4 = { CARRIERS[9], CARRIERS[12], CARRIERS[14], CARRIERS[16] };
//wire [5:0] Carriers6 = { CARRIERS[1], CARRIERS[3], CARRIERS[5], CARRIERS[7], CARRIERS[11], CARRIERS[13] };
//wire [6:0] Carriers7 = { CARRIERS[0], CARRIERS[2], CARRIERS[4], CARRIERS[6], CARRIERS[8], CARRIERS[10], CARRIERS[15] };

// -- Generators.
BitStreamGenerator #(4) gen2(CLOCK, 5'b01111, CARRIERS[16:13], STREAMS[2]);
BitStreamGenerator #(6) gen1(CLOCK, 7'b0110011, CARRIERS[12:7], STREAMS[1]);
BitStreamGenerator #(7) gen0(CLOCK, 8'b01010101, CARRIERS[6:0], STREAMS[0]);

endmodule
