/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Stochastic to Binary Probability Converter
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module BitStreamAccumulator(RESET, CLOCK, STREAM, PROBABILITY, READY);

// -- The binary probability width.
parameter WIDTH = 4;

// -- The inputs.
input logic RESET;
input logic CLOCK;
input bit STREAM;

// -- The outputs.
output bit [WIDTH:0] PROBABILITY;
output wire READY;

// -- Tick counter.
bit [WIDTH-1:0] Counter;
bit Completed;

always @(posedge CLOCK)
    { Completed, Counter } <= RESET ? 1'b0 : Counter + 1'b1;
	 
assign READY = Completed;
    
// -- Accumulates bits from the bit-stream.
bit [WIDTH:0] Accumulator;

always @(posedge CLOCK)
    Accumulator <= RESET ? 1'b0 : Completed ? STREAM : Accumulator + STREAM;
    
// -- Outputs the probability after every cycle.
always @(posedge CLOCK)
    PROBABILITY <= RESET ? 1'b0 : Completed ? Accumulator : PROBABILITY;

endmodule
