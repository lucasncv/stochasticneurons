/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Accumulates a bit-stream.
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module StreamAccumulator #(W)
(
	input wire iClock,
	input wire iReset,
	input logic iStream,
	output logic [W:1] oData
);

// -- The total width necessary for maximum accuracy.
localparam TW = 2 * W - 2;

// -- Accumulates bits from the bit-stream.
logic [TW:1] Accumulator = '0;

wire [TW+1:1] NextAccumulator = Accumulator + iStream;

always @(posedge iClock)
	Accumulator <= iReset ? '0 : NextAccumulator[1+:TW];
    
// -- The current accumulated value.
assign oData = Accumulator[1 + TW - W +: W];

endmodule
