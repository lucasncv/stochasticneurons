/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Linear Feedback Shift Register of length 32
* Author: Lucas Neves Carvalho (100% TESTED!)
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module LFSR32 #(INITIAL = 32'h25AC8713)
(
	input wire iClock,
	output logic [31:0] oTaps
);

/*
* Equidistant seeds for a 32-bit LFSR:
*
* 0x2cdef685, 0xcd184457, 0xec94c4a1, 0xbd4b2fe9, 0xad12409c, 0xb5a760c1, 0xcba1bfc9, 0xb89057fa, 
* 0xfca22d18, 0x0f77bba1, 0xf2b74b2f, 0x204db609, 0xfbe70235, 0xe2626fc3, 0xb7c4dc70, 0x25ac8713
*/

// -- Internal state
bit [32:1] State = INITIAL;

// -- Uncorrelated bit-streams of p=0.5
assign oTaps = State;

// -- Output bit
wire OutputBit = State[32] ^ State[30] ^ State[26] ^ State[25];

// -- Next state
always_ff @(posedge iClock)
    State <= { State[31:1], OutputBit  }; 

endmodule
