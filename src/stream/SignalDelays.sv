/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Delays a signal multiple times.
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module SignalDelays(CLOCK, INPUT, OUTPUTS);

// -- The signal width.
parameter WIDTH = 4;

// -- The delay count.
parameter DELAYS = 1;

// -- The inputs.
input wire CLOCK;
input wire [WIDTH:1] INPUT;

// -- The outputs.
output reg [WIDTH:1] OUTPUTS [1:DELAYS];

// -- First delay.
always @(posedge CLOCK)
	OUTPUTS[1] <= INPUT;

// -- Next delays.
generate
if (DELAYS > 1)
	always @(posedge CLOCK)
		OUTPUTS[2:DELAYS] <= OUTPUTS[1:DELAYS-1];
endgenerate

endmodule
