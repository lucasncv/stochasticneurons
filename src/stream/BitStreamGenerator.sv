/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Stochastic Bit-Stream Generator
* Author: Lucas Neves Carvalho (100% TESTED!)
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module BitStreamGenerator(CLOCK, PROBABILITY, CARRIERS, STREAM);

// -- The binary probability width.
parameter WIDTH = 4;

// -- The inputs.
input wire CLOCK;
input wire [WIDTH:0] PROBABILITY;
input wire [WIDTH:1] CARRIERS;

// -- The outputs.
output wire STREAM;

// -- The stages connections.
wire [WIDTH:1] StagesOutputs;
wire [WIDTH:1] StagesInputs = { StagesOutputs[WIDTH-1:1], 1'b0 };

assign STREAM = PROBABILITY[WIDTH] | StagesOutputs[WIDTH];

// -- Reverse carriers.
function [WIDTH:1] reverseBits(input [WIDTH:1] data);
	integer i;
	for (i = 1; i <= WIDTH; i = i + 1)
		reverseBits[WIDTH + 1 - i] = data[i];
endfunction

wire [WIDTH:1] ReversedCarriers = reverseBits(CARRIERS);

// -- The bit-stream modulators.
BitStreamModulator Modulators[WIDTH:1] (CLOCK, StagesInputs, PROBABILITY[0+:WIDTH], ReversedCarriers, StagesOutputs);

endmodule

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module BitStreamModulator(CLOCK, INPUT, MODULATION, CARRIER, OUTPUT);

// -- The inputs.
input wire CLOCK;
input wire INPUT;
input wire MODULATION;
input wire CARRIER;

// -- The outputs.
output reg OUTPUT;

// -- The modulator logic.
always @(posedge CLOCK)
    OUTPUT <= MODULATION == 1'b1 ? (INPUT | CARRIER) : (INPUT & CARRIER);

endmodule
