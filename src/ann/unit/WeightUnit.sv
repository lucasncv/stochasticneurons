/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module WeightUnit #(N)
(
	input wire iClock,
	input logic [2:0] iGenerationSeed,
	input logic iLearningFactor,
	input logic iDeltaWeights [N],
	output logic oWeights [N],
	NeuralNetworkControl Control
);
	
// -- LFSR for resetting weights.
wire [31:0] LFSRTaps;
LFSR32 #(32'he2626fc3) lfsr(.iClock, .oTaps(LFSRTaps));

wire [7:0] RandomWeight = LFSRTaps[7:0];

// -- Memory access.
IByteMemory #(.N(N)) BankAccess ();
wire [7:0] RAMData;

assign BankAccess.WriteEnable = Control.In(Operation::RESET) || Control.In(Operation::RAM2BANK);
assign BankAccess.WriteData = Control.In(Operation::RESET) ? RandomWeight : RAMData;

// -- Memory sweep.
wire SweepMemory = Control.In(Operation::RESET) || Control.In(Operation::RAM2BANK) || Control.In(Operation::BANK2RAM);
wire FinishedSweeping = BankAccess.Address == N - 1;

// -- Address Control.
wire [9:0] NextAddress = Control.SweepingMemory ? BankAccess.Address + 1 : '0;

always @(posedge iClock)
	BankAccess.Address <= NextAddress;

// -- Internal RAM.
wire [9:0] RAMAddress = Control.In(Operation::RAM2BANK) ? NextAddress : BankAccess.Address;
wire RAMWriteEnable = Control.In(Operation::BANK2RAM);

WeightMemory ram
(
	.address(RAMAddress),
	.clock(iClock),
	.data(BankAccess.ReadData),
	.wren(RAMWriteEnable),
	.q(RAMData)
);

// -- Internal Bank.
wire Learn = Control.In(Operation::LEARN);
wire FinishedLearning;

WeightBank #(.N(N)) bank
(
	.iClock,
	.iLearn(Learn),
	.iRandomSource(LFSRTaps[7]),
	.iGenerationSeed,
	.iLearningFactor,
	.iDeltaWeights,
	.oWeights,
	.oLearned(FinishedLearning),
	.DirectAccess(BankAccess)
);

// -- Busy?
assign Control.Learning = Learn && !FinishedLearning;
assign Control.SweepingMemory = SweepMemory && !FinishedSweeping;

endmodule
