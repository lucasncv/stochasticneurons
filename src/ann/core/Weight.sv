/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module SingleWeight
(
	input wire iClock,
	input logic [2:0] iGenerationSeed,
	output logic oWeight,
	input logic iWriteEnable,
	input wire [7:0] iData,
	output wire [7:0] oData
);

// -- The current weight.
logic [7:0] BinaryWeight = '0;

// -- Outputs the current weight bit-stream.
assign oWeight = BinaryWeight[iGenerationSeed];

// -- Outputs the current 8-bit weight.
assign oData = BinaryWeight;

// -- Updates the current weight.
always @(posedge iClock)
	if (iWriteEnable) BinaryWeight <= iData;

endmodule
