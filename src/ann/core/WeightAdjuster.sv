/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module SingleWeightAdjuster
(
	input wire iClock,
	input wire iEnable,
	input wire iRandomSource,
	input logic iDelta,
	input logic iWeight,
	output logic [7:0] oWeight
);

// -- Calculate new weight.
logic HalfWeight;

StochasticAdder adder
(
	.iA(iDelta),
	.iB(iWeight),
	.iRandomSource,
	.oStream(HalfWeight)
);

// -- Accumulates the new weight's stream.
logic [7:0] BinaryHalfWeight;

StreamAccumulator #(8) acc
(
	.iClock,
	.iReset(~iEnable),
	.iStream(HalfWeight),
	.oData(BinaryHalfWeight)
);

// -- Multiply weight by two.
wire WillOverflow = BinaryHalfWeight[7] == BinaryHalfWeight[6];
wire [7:0] Saturated = { 8 { BinaryHalfWeight[7] } };
wire [7:0] TimesTwo = { ~BinaryHalfWeight[6], BinaryHalfWeight[5:0], BinaryHalfWeight[0] }; // unbiased drifting

assign oWeight = WillOverflow ? Saturated : TimesTwo;

endmodule

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
//module BinaryWeightAdjuster
//(
//	input wire iClock,
//	input wire iEnable,
//	input logic iDelta,
//	input logic [7:0] iBinaryWeight,
//	output logic [7:0] oBinaryWeight
//);
//
//// -- Threshold.
//logic [2:0] State = '0;
//wire Threshold = State[2];
//
//always @(posedge iClock)
//case ({ iDelta, State})
//	4'b0000: State <= 3'b000;
//	4'b0001: State <= 3'b000;
//	4'b0010: State <= 3'b001;
//	4'b0011: State <= 3'b010;
//	4'b0100: State <= 3'b011;
//	4'b0101: State <= 3'b100;
//	4'b0110: State <= 3'b101;
//	4'b0111: State <= 3'b110;
//	
//	4'b1000: State <= 3'b001;
//	4'b1001: State <= 3'b010;
//	4'b1010: State <= 3'b011;
//	4'b1011: State <= 3'b100;
//	4'b1100: State <= 3'b101;
//	4'b1101: State <= 3'b110;
//	4'b1110: State <= 3'b111;
//	4'b1111: State <= 3'b111;
//endcase
//
//// -- Simple accumulator.
//logic [7:0] Estimative;
//
//wire [1:0] Comparison = { Estimative[7], Estimative[4] };
//
//StreamAccumulator #(8) acc
//(
//	.iClock,
//	.iReset(~iEnable),
//	.iStream(Threshold),
//	.oData(Estimative)
//);
//
//// -- New values.
//wire [7:0] IncreasedWeight = iBinaryWeight + 'd4;
//wire [7:0] DecreasedWeight = iBinaryWeight - 'd4;
//
//// -- Delta.
//always_comb case (Comparison)
//	2'b00:
//		oBinaryWeight = DecreasedWeight[7] && !iBinaryWeight[7] ? 8'h00 : DecreasedWeight;
//	2'b11:
//		oBinaryWeight = iBinaryWeight[7] && !IncreasedWeight[7] ? 8'hFF : IncreasedWeight;
//	default:
//		oBinaryWeight = iBinaryWeight;
//endcase
//
//endmodule

module BinaryWeightAdjuster
(
	input wire iClock,
	input wire iEnable,
	input logic iDelta,
	input logic [7:0] iBinaryWeight,
	output logic [7:0] oBinaryWeight
);

// -- Simple accumulator.
logic [7:0] BinaryDelta;

StreamAccumulator #(8) acc
(
	.iClock,
	.iReset(~iEnable),
	.iStream(iDelta),
	.oData(BinaryDelta)
);

// -- Apply threshold.
wire [1:0] Comparison = { BinaryDelta > 8'd130, BinaryDelta < 8'd126 };

// -- New values.
wire [7:0] IncreasedWeight = iBinaryWeight + 'd4;
wire [7:0] DecreasedWeight = iBinaryWeight - 'd4;

// -- Delta.
always_comb case (Comparison)
	2'b01:
		oBinaryWeight = DecreasedWeight[7] && !iBinaryWeight[7] ? 8'h00 : DecreasedWeight;
	2'b10:
		oBinaryWeight = iBinaryWeight[7] && !IncreasedWeight[7] ? 8'hFF : IncreasedWeight;
	default:
		oBinaryWeight = iBinaryWeight;
endcase

endmodule

