/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module SingleNeuron
(
	input wire iClock,
	input logic iRandomSource,
	input logic iNetInput,
	input logic iNetError,
	output logic oActivation,
	output logic oLocalError
);

// -- The derivative of the activation function.
wire Derivative;

// -- Activation function.
StochasticTanh activation(.iClock, .iRandomSource, .iStream(iNetInput), .oActivation, .oDerivative(Derivative));

// -- Calculate local error.
always_ff @(posedge iClock)
	oLocalError <= ~(Derivative ^ iNetError);

endmodule
