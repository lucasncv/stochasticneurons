/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* A Neural Network.
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module NeuralNetworkWithTwoHiddenLayers
(
	input wire iClock,
	input logic iLearningFactor,
	input logic iInputs [I],
	input logic iExpectedOutputs [O],
	output logic oActualOutputs [O],
	output logic oPerformance [O],
	NeuralNetworkControl Control
);

// -- Generic parameters.
localparam InputSize = 2;
localparam HiddenSize = 8;
localparam OutputSize = 1;

// -- Derived parameters.
localparam InputToHiddenCount = (InputSize + 1) * HiddenSize;
localparam HiddenToHiddenCount = (HiddenSize + 1) * HiddenSize;
localparam HiddenToOutputCount = (HiddenSize + 1) * OutputSize;

localparam Synap0 = 0;
localparam Synap1 = Synap0 + InputToHiddenCount;
localparam Synap2 = Synap1 + HiddenToHiddenCount;

// -- More derived parameters.
localparam I = InputSize;
localparam O = OutputSize;
localparam WN = InputToHiddenCount + HiddenToHiddenCount + HiddenToOutputCount;

// -- Linear Feedback Shift Register.
bit [31:0] RandomStreams [7];

LFSR32 #(32'h2cdef685) lfsr0(.iClock, .oTaps(RandomStreams[0]));
LFSR32 #(32'hcd184457) lfsr1(.iClock, .oTaps(RandomStreams[1]));
LFSR32 #(32'hec94c4a1) lfsr2(.iClock, .oTaps(RandomStreams[2]));
LFSR32 #(32'hbd4b2fe9) lfsr3(.iClock, .oTaps(RandomStreams[3]));
LFSR32 #(32'had12409c) lfsr4(.iClock, .oTaps(RandomStreams[4]));
LFSR32 #(32'hb5a760c1) lfsr5(.iClock, .oTaps(RandomStreams[5]));
LFSR32 #(32'hcba1bfc9) lfsr6(.iClock, .oTaps(RandomStreams[6]));

// -- Address generators.
bit [2:0] WeightSeeds;
AddressBitStreamsGenerator addr0(iClock, RandomStreams[0][16:0], WeightSeeds);

// -- Weights.
wire Weights [WN];
wire DeltaWeights [WN];

WeightUnit #(.N(WN)) weights
(
	.iClock,
	.iGenerationSeed(WeightSeeds),
	.iLearningFactor,
	.iDeltaWeights(DeltaWeights),
	.oWeights(Weights),
	.Control(Control)
);

// -- Input Layer.
LayerRightSide #(InputSize) inputLayerRS();

InputLayer #(InputSize) inputLayer
(
	.iStreams(iInputs),
	.NextLayer(inputLayerRS)
);

// -- Hidden Layer.
LayerLeftSide #(HiddenSize) hiddenLayerLS();
LayerRightSide #(HiddenSize) hiddenLayerRS();

HiddenLayer #(HiddenSize) hiddenLayer
(
	.iClock,
	.iRandomSource(RandomStreams[4][0]),
	.PreviousLayer(hiddenLayerLS),
	.NextLayer(hiddenLayerRS)
);

// -- Hidden Layer.
LayerLeftSide #(HiddenSize) hiddenLayer2LS();
LayerRightSide #(HiddenSize) hiddenLayer2RS();

HiddenLayer #(HiddenSize) hiddenLayer2
(
	.iClock,
	.iRandomSource(RandomStreams[5][0]),
	.PreviousLayer(hiddenLayer2LS),
	.NextLayer(hiddenLayer2RS)
);

// -- Output Layer.
LayerLeftSide #(OutputSize) outputLayerLS();

OutputLayer #(OutputSize) outputLayer
(
	.iClock,
	.iRandomSource(RandomStreams[6][0]),
	.PreviousLayer(outputLayerLS),
	.iExpectedOutputs,
	.oActualOutputs,
	.oPerformance
);

// -- Synapses.
Synapses #(.M(InputSize), .N(HiddenSize)) synapses0
(
	.iClock,
	.iRandomSource(RandomStreams[1][0]),
	.iWeights(Weights[Synap0+:InputToHiddenCount]),
	.oDeltaWeights(DeltaWeights[Synap0+:InputToHiddenCount]),
	.PreviousLayer(inputLayerRS),
	.NextLayer(hiddenLayerLS)
);

Synapses #(.M(HiddenSize), .N(HiddenSize)) synapses1
(
	.iClock,
	.iRandomSource(RandomStreams[2][0]),
	.iWeights(Weights[Synap1+:HiddenToHiddenCount]),
	.oDeltaWeights(DeltaWeights[Synap1+:HiddenToHiddenCount]),
	.PreviousLayer(hiddenLayerRS),
	.NextLayer(hiddenLayer2LS)
);

Synapses #(.M(HiddenSize), .N(OutputSize)) synapses2
(
	.iClock,
	.iRandomSource(RandomStreams[3][0]),
	.iWeights(Weights[Synap2+:HiddenToOutputCount]),
	.oDeltaWeights(DeltaWeights[Synap2+:HiddenToOutputCount]),
	.PreviousLayer(hiddenLayer2RS),
	.NextLayer(outputLayerLS)
);

endmodule
