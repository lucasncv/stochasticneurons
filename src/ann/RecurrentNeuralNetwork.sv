/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* A Recurrent Neural Network.
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module RecurrentNeuralNetwork
(
	input wire iClock,
	input logic iLearningFactor,
	input logic iInputs [I],
	input logic iExpectedOutputs [O],
	output logic oActualOutputs [O],
	output logic oPerformance [O],
	NeuralNetworkControl Control
);

// -- Generic parameters.
localparam InputSize = 1;
localparam HiddenSize = 5;
localparam OutputSize = 1;

// -- Derived parameters.
localparam ContextCount = HiddenSize * HiddenSize;
localparam InputToHiddenCount = (InputSize + 1) * HiddenSize + ContextCount;
localparam HiddenToOutputCount = (HiddenSize + 1) * OutputSize;

// -- More derived parameters.
localparam I = InputSize;
localparam O = OutputSize;
localparam WN = InputToHiddenCount + HiddenToOutputCount;

// -- Linear Feedback Shift Register.
bit [31:0] RandomStreams [6];

LFSR32 #(32'h2cdef685) lfsr0(.iClock, .oTaps(RandomStreams[0]));
LFSR32 #(32'hcd184457) lfsr1(.iClock, .oTaps(RandomStreams[1]));
LFSR32 #(32'hec94c4a1) lfsr2(.iClock, .oTaps(RandomStreams[2]));
LFSR32 #(32'hbd4b2fe9) lfsr3(.iClock, .oTaps(RandomStreams[3]));
LFSR32 #(32'had12409c) lfsr4(.iClock, .oTaps(RandomStreams[4]));
LFSR32 #(32'hb5a760c1) lfsr5(.iClock, .oTaps(RandomStreams[5]));

// -- Address generators.
bit [2:0] WeightSeeds;
bit [2:0] ContextSeeds;

AddressBitStreamsGenerator addr0(iClock, RandomStreams[0][16:0], WeightSeeds);
AddressBitStreamsGenerator addr1(iClock, RandomStreams[5][16:0], ContextSeeds);

// -- Weights.
wire Weights [WN];
wire DeltaWeights [WN];

WeightUnit #(.N(WN)) weights
(
	.iClock,
	.iGenerationSeed(WeightSeeds),
	.iLearningFactor,
	.iDeltaWeights(DeltaWeights),
	.oWeights(Weights),
	.Control
);

// -- Input Layer.
LayerRightSide #(InputSize) inputLayerRS();

InputLayer #(InputSize) inputLayer
(
	.iStreams(iInputs),
	.NextLayer(inputLayerRS)
);

// -- Hidden Layer.
LayerLeftSide #(HiddenSize) hiddenLayerLS();
LayerRightSide #(HiddenSize) hiddenLayerRS();

HiddenLayer #(HiddenSize) hiddenLayer
(
	.iClock,
	.iRandomSource(RandomStreams[3][0]),
	.PreviousLayer(hiddenLayerLS),
	.NextLayer(hiddenLayerRS)
);

// -- Context Layer.
LayerRightSide #(HiddenSize) contextLayerRS();

ContextLayer #(HiddenSize) contextLayer
(
	.iClock,
	.Control,
	.iGenerationSeed(ContextSeeds),
	.SourceLayer(hiddenLayerRS),
	.NextLayer(contextLayerRS)
);

// -- Output Layer.
LayerLeftSide #(OutputSize) outputLayerLS();

OutputLayer #(OutputSize) outputLayer
(
	.iClock,
	.iRandomSource(RandomStreams[4][0]),
	.PreviousLayer(outputLayerLS),
	.iExpectedOutputs,
	.oActualOutputs,
	.oPerformance
);

// -- Combine inputs and context activations.
LayerRightSide #(InputSize + HiddenSize) combinedLayerRS();

LayerRightSideCombiner #(InputSize, HiddenSize) inputAndContextCombiner
(
	.FirstLayer(inputLayerRS),
	.SecondLayer(contextLayerRS),
	.CombinedLayer(combinedLayerRS)
);

// -- Synapses.
Synapses #(.M(InputSize + HiddenSize), .N(HiddenSize)) synapses0
(
	.iClock,
	.iRandomSource(RandomStreams[1][0]),
	.iWeights(Weights[0+:InputToHiddenCount]),
	.oDeltaWeights(DeltaWeights[0+:InputToHiddenCount]),
	.PreviousLayer(combinedLayerRS),
	.NextLayer(hiddenLayerLS)
);

Synapses #(.M(HiddenSize), .N(OutputSize)) synapses1
(
	.iClock,
	.iRandomSource(RandomStreams[2][0]),
	.iWeights(Weights[InputToHiddenCount+:HiddenToOutputCount]),
	.oDeltaWeights(DeltaWeights[InputToHiddenCount+:HiddenToOutputCount]),
	.PreviousLayer(hiddenLayerRS),
	.NextLayer(outputLayerLS)
);

endmodule
