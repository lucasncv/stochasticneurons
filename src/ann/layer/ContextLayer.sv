/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* A context layer from N stochastic neurons.
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module ContextLayer #(N)
(
	// Control
	input wire iClock,
	NeuralNetworkControl Control,
	
	// Generation
	input logic [2:0] iGenerationSeed,
	
	// Layers
	LayerRightSide.probe SourceLayer,
	LayerRightSide.layer NextLayer
);

// -- Control.
wire Accumulate = Control.In(Operation::CYCLE) || Control.In(Operation::LEARN);
wire InitializeToZero = Control.In(Operation::CLEAR);

wire Updated;
assign Control.UpdatingContext = Accumulate && !Updated;

// -- Address delays.
logic [2:0] Seeds [N];

DelayChain #(.WIDTH(3), .N(N)) seedDelays
(
	.iClock,
	.iSignal(iGenerationSeed),
	.oSignal(Seeds)
);

// -- Control accumulators.
StreamAccumulatorControl #(8) accControl
(
	.iClock,
	.iReset(~Accumulate),
	.oReady(Updated)
);

generate genvar n;
	for (n = 0; n < N; n++) begin : ForEachNeuron
	
		// -- Accumulate activations.
		logic [7:0] Accumulator;

		StreamAccumulator #(8) acc
		(
			.iClock,
			.iReset(~Accumulate),
			.iStream(SourceLayer.Activations[n]),
			.oData(Accumulator)
		);
		
		// -- Store delayed activation.
		logic [7:0] BinaryActivation;
		
		always_ff @(posedge iClock)
			if (InitializeToZero)
				BinaryActivation <= 8'h80;
			else if (Updated)
				BinaryActivation <= Accumulator;

		// -- Generation.
		assign NextLayer.Activations[n] = BinaryActivation[Seeds[n]];

	end
endgenerate

endmodule
