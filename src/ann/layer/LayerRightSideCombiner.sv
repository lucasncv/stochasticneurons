/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Combines outputs from two layers into a single interface.
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module LayerRightSideCombiner #(M, N)
(
	LayerRightSide.synapses FirstLayer,
	LayerRightSide.synapses SecondLayer,
	LayerRightSide.layer CombinedLayer
);

// -- Activations.
assign CombinedLayer.Activations[0+:M] = FirstLayer.Activations;
assign CombinedLayer.Activations[M+:N] = SecondLayer.Activations;

// -- Errors.
assign FirstLayer.NetErrors = CombinedLayer.NetErrors[0+:M];
assign SecondLayer.NetErrors = CombinedLayer.NetErrors[M+:N];

endmodule
