/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* A hidden layer of N stochastic neurons.
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module HiddenLayer #(N)
(
	input wire iClock,
	input logic iRandomSource,
	LayerLeftSide.layer PreviousLayer,
	LayerRightSide.layer NextLayer
);

generate genvar n;
	for (n = 0; n < N; n++) begin : ForEachNeuron

		// -- A neuron.
		SingleNeuron neurons
		(
			.iClock,
			.iRandomSource,
			.iNetInput(PreviousLayer.NetInputs[n]),
			.iNetError(NextLayer.NetErrors[n]),
			.oActivation(NextLayer.Activations[n]),
			.oLocalError(PreviousLayer.LocalErrors[n])
		);

	end
endgenerate

endmodule
