/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module InputLayer #(N)
(
	input logic iStreams [N],
	LayerRightSide.layer NextLayer
);

// -- Present the input data to the network.
assign NextLayer.Activations = iStreams;

endmodule
