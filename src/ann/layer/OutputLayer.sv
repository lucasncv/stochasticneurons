/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module OutputLayer #(N)
(
	input wire iClock,
	input logic iRandomSource,
	LayerLeftSide.layer PreviousLayer,
	input logic iExpectedOutputs [N],
	output logic oActualOutputs [N],
	output logic oPerformance [N]
);

// -- Random source delays.
logic RandomSources [N * 2 + 1];

DelayChain #(.WIDTH(1), .N(N * 2 + 1)) delays
(
	.iClock,
	.iSignal(iRandomSource),
	.oSignal(RandomSources)
);

// -- The output deltas.
wire OutputDelta [N];
assign oPerformance = OutputDelta;

// -- For each neuron.
generate genvar n;
	for (n = 0; n < N; n++) begin : ForEachNeuron

		// -- The neuron.
		SingleNeuron neuron
		(
			.iClock,
			.iRandomSource(RandomSources[n * 2]),
			.iNetInput(PreviousLayer.NetInputs[n]),
			.iNetError(OutputDelta[n]),
			.oActivation(oActualOutputs[n]),
			.oLocalError(PreviousLayer.LocalErrors[n])
		);
		
		// -- The output delta calculator.
		StochasticAdder adder
		(
			.iA(iExpectedOutputs[n]),
			.iB(~oActualOutputs[n]),
			.iRandomSource(RandomSources[n * 2 + 1]),
			.oStream(OutputDelta[n])
		);
		
	end
endgenerate

endmodule
