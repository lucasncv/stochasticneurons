/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module ConstantWeightUnit #(N)
(
	input wire iClock,
	input logic [2:0] iGenerationSeed,
	input logic iLearningFactor,
	input logic iDeltaWeights [N],
	output logic oWeights [N],
	NeuralNetworkControl Control
);

// -- Address delays.
logic [2:0] Seeds [N];

DelayChain #(.WIDTH(3), .N(N)) seedDelays
(
	.iClock,
	.iSignal(iGenerationSeed),
	.oSignal(Seeds)
);

// -- Weights.
logic [7:0] BinaryWeights [N] = '{
	8'h00,
	8'h00,
	8'hFF,
	8'hFF,
	8'h00,
	8'h00,
	8'h00,
	8'h00,
	8'h00
};

// -- Stochastic generation.
generate genvar n;
for (n = 0; n < N; n++) begin : ForEachN

	// -- Outputs the weight's bit-stream.
	assign oWeights[n] = BinaryWeights[n][Seeds[n]];

end : ForEachN
endgenerate

endmodule
