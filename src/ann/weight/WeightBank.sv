/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module WeightBank #(N)
(
	input wire iClock,
	input wire iLearn,
	input logic iRandomSource,
	input logic [2:0] iGenerationSeed,
	input logic iLearningFactor,
	input logic iDeltaWeights [N],
	output logic oWeights [N],
	output logic oLearned,
	IByteMemory DirectAccess
);

// -- Address delays.
logic [2:0] Seeds [N];

DelayChain #(.WIDTH(3), .N(N)) seedDelays
(
	.iClock,
	.iSignal(iGenerationSeed),
	.oSignal(Seeds)
);

// -- Random source delays.
//logic RandomSources [N];
//
//DelayChain #(.WIDTH(1), .N(N)) randomSourceDelays
//(
//	.iClock,
//	.iSignal(iRandomSource),
//	.oSignal(RandomSources)
//);

// -- Control accumulators.
StreamAccumulatorControl #(8) accControl
(
	.iClock,
	.iReset(~iLearn),
	.oReady(oLearned)
);

// -- Memory access.
wire [7:0] BinaryWeightData [N];
assign DirectAccess.ReadData = BinaryWeightData[DirectAccess.Address];

/* --- The section below is generated circuit --- */
generate genvar n, m;

// -- Instantiate every weight and its adjuster.
for (n = 0; n < N; n++) begin : ForEachN

	// -- Adjusted data source.
	wire [7:0] AdjustedDataSource;

	// -- Handle access control.
	wire WriteEn = oLearned || (DirectAccess.Address == n && DirectAccess.WriteEnable);
	
	// -- Handle data i/o.
	wire [7:0] WriteData = oLearned ? AdjustedDataSource : DirectAccess.WriteData;

	// -- A single weight.
	SingleWeight weight
	(
		.iClock,
		.iGenerationSeed(Seeds[n]),
		.oWeight(oWeights[n]),
		.iWriteEnable(WriteEn),
		.iData(WriteData),
		.oData(BinaryWeightData[n])
	);
	
	// -- Learning factor.
	wire DeltaWeight = iDeltaWeights[n];//~( ^ iLearningFactor);
	
	// -- A single weight adjuster.
//	SingleWeightAdjuster weightAdjuster
//	(
//		.iClock,
//		.iEnable(iLearn & ~oLearned),
//		.iRandomSource(RandomSources[n]),
//		.iDelta(DeltaWeight),
//		.iWeight(oWeights[n]),
//		.oWeight(AdjustedDataSource)
//	);

	// -- Binary adjustment.
	BinaryWeightAdjuster weightAdjuster
	(
		.iClock,
		.iEnable(iLearn),
		.iDelta(DeltaWeight),
		.iBinaryWeight(BinaryWeightData[n]),
		.oBinaryWeight(AdjustedDataSource)
	);
		
end : ForEachN

endgenerate
/* --- End of generated section --- */

endmodule

