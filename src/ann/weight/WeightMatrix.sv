/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module WeightVectorToMatrix #(M, N)
(
	input logic iVector [M * N + N],
	output logic oMatrix [N][M],
	output logic oBiases [N]
);

generate genvar n, m;
	for (n = 0; n < N; n++) begin : ForEachN
	
		// -- Biases.
		assign oBiases[n] = iVector[N * M + n];
		
		// -- Weights.
		for (m = 0; m < M; m++) begin : ForEachM
			assign oMatrix[n][m] = iVector[n * M + m];
		end
		
	end
endgenerate

endmodule

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module WeightMatrixToVector #(M, N)
(
	input logic iMatrix [N][M],
	input logic iBiases [N],
	output logic oVector [M * N + N]
);

generate genvar n, m;
	for (n = 0; n < N; n++) begin : ForEachN
	
		// -- Biases.
		assign oVector[N * M + n] = iBiases[n];
		
		// -- Weights.
		for (m = 0; m < M; m++) begin : ForEachM
			assign oVector[n * M + m] = iMatrix[n][m];
		end
		
	end
endgenerate

endmodule
