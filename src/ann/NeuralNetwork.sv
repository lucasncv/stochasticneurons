/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* A Neural Network.
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module NeuralNetwork
(
	input wire iClock,
	input logic iLearningFactor,
	input logic iInputs [I],
	input logic iExpectedOutputs [O],
	output logic oActualOutputs [O],
	output logic oPerformance [O],
	NeuralNetworkControl Control
);

// -- Generic parameters.
localparam InputSize = 2;
localparam HiddenSize = 4;
localparam OutputSize = 1;

// -- Derived parameters.
localparam InputToHiddenCount = (InputSize + 1) * HiddenSize;
localparam HiddenToOutputCount = (HiddenSize + 1) * OutputSize;

// -- More derived parameters.
localparam I = InputSize;
localparam O = OutputSize;
localparam WN = InputToHiddenCount + HiddenToOutputCount;

// -- Linear Feedback Shift Register.
bit [31:0] RandomStreams [5];

LFSR32 #(32'h2cdef685) lfsr0(.iClock, .oTaps(RandomStreams[0]));
LFSR32 #(32'hcd184457) lfsr1(.iClock, .oTaps(RandomStreams[1]));
LFSR32 #(32'hec94c4a1) lfsr2(.iClock, .oTaps(RandomStreams[2]));
LFSR32 #(32'hbd4b2fe9) lfsr3(.iClock, .oTaps(RandomStreams[3]));
LFSR32 #(32'had12409c) lfsr4(.iClock, .oTaps(RandomStreams[4]));

// -- Address generators.
bit [2:0] WeightSeeds;
AddressBitStreamsGenerator addr0(iClock, RandomStreams[0][16:0], WeightSeeds);

// -- Weights.
wire Weights [WN];
wire DeltaWeights [WN];

WeightUnit #(.N(WN)) weights
(
	.iClock,
	.iGenerationSeed(WeightSeeds),
	.iLearningFactor,
	.iDeltaWeights(DeltaWeights),
	.oWeights(Weights),
	.Control(Control)
);

// -- Input Layer.
LayerRightSide #(InputSize) inputLayerRS();

InputLayer #(InputSize) inputLayer
(
	.iStreams(iInputs),
	.NextLayer(inputLayerRS)
);

// -- Hidden Layer.
LayerLeftSide #(HiddenSize) hiddenLayerLS();
LayerRightSide #(HiddenSize) hiddenLayerRS();

HiddenLayer #(HiddenSize) hiddenLayer
(
	.iClock,
	.iRandomSource(RandomStreams[3][0]),
	.PreviousLayer(hiddenLayerLS),
	.NextLayer(hiddenLayerRS)
);

// -- Output Layer.
LayerLeftSide #(OutputSize) outputLayerLS();

OutputLayer #(OutputSize) outputLayer
(
	.iClock,
	.iRandomSource(RandomStreams[4][0]),
	.PreviousLayer(outputLayerLS),
	.iExpectedOutputs,
	.oActualOutputs,
	.oPerformance
);

// -- Synapses.
Synapses #(.M(InputSize), .N(HiddenSize)) synapses0
(
	.iClock,
	.iRandomSource(RandomStreams[1][0]),
	.iWeights(Weights[0+:InputToHiddenCount]),
	.oDeltaWeights(DeltaWeights[0+:InputToHiddenCount]),
	.PreviousLayer(inputLayerRS),
	.NextLayer(hiddenLayerLS)
);

Synapses #(.M(HiddenSize), .N(OutputSize)) synapses1
(
	.iClock,
	.iRandomSource(RandomStreams[2][0]),
	.iWeights(Weights[InputToHiddenCount+:HiddenToOutputCount]),
	.oDeltaWeights(DeltaWeights[InputToHiddenCount+:HiddenToOutputCount]),
	.PreviousLayer(hiddenLayerRS),
	.NextLayer(outputLayerLS)
);

endmodule
