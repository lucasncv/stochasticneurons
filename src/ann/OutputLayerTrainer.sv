/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Stochastic trainer module for output layer neurons.
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module OutputLayerTrainer
(
	input logic iRandomSource,
	input logic iDerivative,
	input logic iActualOutput,
	input logic iExpectedOutput,
	output logic oLocalError,
	output logic oPerformance
);

// -- Output delta.
logic Delta;
StochasticAdder adder(.iA(iExpectedOutput), .iB(~iActualOutput), .iRandomSource, .oStream(Delta));

// -- Local error function.
assign oLocalError = ~(iDerivative ^ Delta);

// -- Performance.
assign oPerformance = Delta;

endmodule
