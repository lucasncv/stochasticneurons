/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Stochastic neuron module. (With embedded synapses)
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module CompleteNeuron(CLOCK, WEIGHTS, LUTADDRESSES, INPUTS, OUTPUT, DERIVATIVE);

// -- The number of inputs to the neuron.
parameter N = 2;

// -- The inputs.
input wire CLOCK;
input wire [7:0] WEIGHTS [1:N];
input wire [2:0] LUTADDRESSES [1:N];
input wire [N:1] INPUTS;

// -- The outputs.
output logic OUTPUT;
output logic DERIVATIVE;

// -- Weighted inputs.
logic [1:N] SynapseOutputs;

// -- Synapses.
Synapse synapses [1:N] (WEIGHTS, LUTADDRESSES, INPUTS, SynapseOutputs);

// -- Weighted sum.
logic SumOfSynapses;

StochasticGenericAdder #(N) adder(CLOCK, SynapseOutputs, SumOfSynapses);

// -- Activation function.
StochasticTanh activation(CLOCK, SumOfSynapses, OUTPUT, DERIVATIVE);

endmodule
