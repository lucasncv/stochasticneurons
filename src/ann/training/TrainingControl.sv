/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Controls the training of a Neural Network.
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module TrainingControl #(I, O)
(
	input wire iClock,
	input wire iReset,
	input logic iNext,
	output logic oReady,
	output logic oFirstSet,
	output logic oInputSet [I],
	output logic oOutputSet [O]
);

// -- State.
typedef enum logic [1:0] { RESET, READY, INPUTS, OUTPUTS } StateCodes;
StateCodes State = RESET;

logic [7:0] StateTick = '0;
assign oReady = State == READY;

// -- Memory access.
logic [11:0] InputAddress = '0;
logic [11:0] OutputAddress = '0;
wire [7:0] InputRead;


// -- The set memory.
InputSetMemory memInput
(
	.address(iReset ? '0 : InputAddress),
	.clock(iClock),
	.q(InputRead)
);

//wire OutputRead;
//OutputSetMemory memOutput
wire [7:0] OutputRead;
OutputSetMemory8bit memOutput
(
	.address(iReset ? '0 : OutputAddress),
	.clock(iClock),
	.q(OutputRead)
);

// -- Set.
logic [7:0] InputSet [I];
logic [7:0] OutputSet [O];

logic [7:0] CurrentSet = '0;
logic [7:0] SetCount = '0;

assign oFirstSet = State == READY && CurrentSet == 1;

// -- Chooses the next state.
StateCodes NextState;

always_comb begin
	NextState = State;
	
	case (State)
		RESET:
			if (SetCount != '0)
				NextState = READY;
		READY:
			if (iNext)
				NextState = INPUTS;
		INPUTS:
			if (StateTick == I - 1)
				NextState = OUTPUTS;
		OUTPUTS:
			if (StateTick == O - 1)
				NextState = READY;
	endcase
	
	if (iReset)
		NextState = RESET;
end

wire InTransition = State != NextState;

// -- Next state logic.
always_ff @(posedge iClock)
	case (NextState)
		RESET: begin
			SetCount <= InTransition ? '0 : InputRead;
			InputAddress <= '0;
			OutputAddress <= '0;
			CurrentSet <= '0;
		end
		READY: begin
			if (InTransition) begin
				if (State == RESET || CurrentSet == SetCount) begin
					InputAddress <= 1;
					OutputAddress <= 0;
					CurrentSet <= 1;
				end else begin
					CurrentSet <= CurrentSet + 1;
				end
			end
		end
		INPUTS: begin
			InputAddress <= InputAddress + 1;
		end
		OUTPUTS: begin
			OutputAddress <= OutputAddress + 1;
		end
	endcase
	
// -- Present state logic.
always_ff @(posedge iClock)
	case (State)
		INPUTS: InputSet[StateTick] <= InputRead;
		OUTPUTS: OutputSet[StateTick] <= OutputRead;
	endcase

// -- Update state.
always_ff @(posedge iClock) begin
	StateTick <= InTransition ? '0 : StateTick + 1;
	State <= NextState;
end

// -- Generators.
wire [2:0] Seeds [I+O];

generate genvar i;
	for (i = 0; i < I; i++) begin : ForEachInput
		assign oInputSet[i] = InputSet[i][Seeds[i]];
	end
	
	for (i = 0; i < O; i++) begin : ForEachOutput
		assign oOutputSet[i] = OutputSet[i][Seeds[i+I]];
	end
endgenerate

//assign oOutputSet = OutputSet;

// -- LFSR for generating inputs.
wire [31:0] LFSRTaps;
LFSR32 #(32'h25ac8713) lfsr(.iClock, .oTaps(LFSRTaps));

// -- Address generators.
bit [2:0] AddressBitStreams;
AddressBitStreamsGenerator addr(iClock, LFSRTaps[16:0], AddressBitStreams);

DelayChain #(.WIDTH(3), .N(I+O)) seedDelays
(
	.iClock,
	.iSignal(AddressBitStreams),
	.oSignal(Seeds)
);

//DebugProbes dbg
//(
//	.probe({ State, NextState, StateTick, InputSet[0], InputSet[1], OutputSet[0], InputAddress, OutputAddress, SetCount[3:0] }),
//	.source_clk(iClock),
//	.source()
//);

endmodule
