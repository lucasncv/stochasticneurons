/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Controls a Neural Network.
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
interface NeuralNetworkControl
(
	input wire iClock,
	input logic [2:0] iOperation,
	output wire oBusy
);

// -- Feedbacks from the network.
wire SweepingMemory;
wire Learning;
wire UpdatingContext;

// -- Control.
Operation::Code CurrentOperation, NextOperation;

always_comb NextOperation = oBusy ? CurrentOperation : Operation::Code'(iOperation);

always @(posedge iClock)
	CurrentOperation <= NextOperation;
	
// -- Busy feedback.
assign oBusy = SweepingMemory | Learning | UpdatingContext;

// -- Helpers.
function bit In (input Operation::Code op);
	return CurrentOperation == op;
endfunction

endinterface

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
package Operation;

// -- Operations.
typedef enum logic [2:0]
{
	NORMAL,		// Normal forward processing.
	LEARN,		// Executes one iteration of the Backpropagation algorithm
	RESET,		// Resets everything, initialize weights to random values
	RAM2BANK,	// Transfers weights from the RAM to the bank
	BANK2RAM,	// Transfers weights form the bank to the RAM
	CYCLE,		// Executes a cycle, updating context layer delays
	CLEAR			// Clears context layers
	
} Code;

endpackage
