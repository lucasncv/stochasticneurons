/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* A Neural Network.
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module NeuralNetworkOfOneNeuron
(
	input wire iClock,
	input logic iLearningFactor,
	input logic iInputs [I],
	input logic iExpectedOutputs [1],
	output logic oActualOutputs [1],
	output logic oPerformance [1],
	NeuralNetworkControl Control
);

// -- Parameters.
localparam I = 2;
localparam WN = I + 1;

// -- Linear Feedback Shift Register.
bit [31:0] RandomStreams [7];

LFSR32 #(32'h2cdef685) lfsr0(.iClock, .oTaps(RandomStreams[0]));
LFSR32 #(32'hcd184457) lfsr1(.iClock, .oTaps(RandomStreams[1]));
LFSR32 #(32'hec94c4a1) lfsr2(.iClock, .oTaps(RandomStreams[2]));

// -- Address generators.
bit [2:0] AddressBitStreams;
AddressBitStreamsGenerator addr(iClock, RandomStreams[0][16:0], AddressBitStreams);

// -- Weights.
wire Weights [WN];
wire DeltaWeights [WN];

WeightUnit #(.N(WN)) weights
(
	.iClock,
	.iGenerationSeed(AddressBitStreams),
	.iLearningFactor,
	.iDeltaWeights(DeltaWeights),
	.oWeights(Weights),
	.Control
);

// -- Input Layer.
LayerRightSide #(I) inputLayerRS();

InputLayer #(I) inputLayer
(
	.iStreams(iInputs),
	.NextLayer(inputLayerRS)
);

// -- Output Layer.
LayerLeftSide #(1) outputLayerLS();

OutputLayer #(1) outputLayer
(
	.iClock,
	.iRandomSource(RandomStreams[1][0]),
	.PreviousLayer(outputLayerLS),
	.iExpectedOutputs,
	.oActualOutputs,
	.oPerformance
);

// -- Synapses.
Synapses #(.M(I), .N(1)) synapses0
(
	.iClock,
	.iRandomSource(RandomStreams[2][0]),
	.iWeights(Weights),
	.oDeltaWeights(DeltaWeights),
	.PreviousLayer(inputLayerRS),
	.NextLayer(outputLayerLS)
);

endmodule
