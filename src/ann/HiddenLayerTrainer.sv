/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Stochastic trainer module for hidden layer neurons.
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module HiddenLayerTrainer(CLOCK, DERIVATIVE, FEEDBACKS, GRADIENT);

// -- The number of synapses the associated neuron stimulates.
parameter N = 2;

// -- The inputs.
input wire CLOCK;
input wire DERIVATIVE;
input wire [N:1] FEEDBACKS;

// -- The outputs.
output wire GRADIENT;

// -- Sum of feedbacks.
logic SumOfFeedbacks;

StochasticGenericAdder #(N) adder (CLOCK, FEEDBACKS, SumOfFeedbacks);

// -- Local error function.
assign GRADIENT = ~(DERIVATIVE ^ SumOfFeedbacks);

endmodule
