/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Stochastic synapse module. (Bipolar)
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module Synapse(WEIGHT, LUTADDRESS, INPUT, OUTPUT);

// -- The inputs.
input wire [7:0] WEIGHT;
input wire [2:0] LUTADDRESS;
input wire INPUT;

// -- The outputs.
output wire OUTPUT;

// -- Weight bit-stream generator.
wire WeightStream = WEIGHT[LUTADDRESS];

// -- Output bit-stream.
assign OUTPUT = ~(INPUT ^ WeightStream);

endmodule
