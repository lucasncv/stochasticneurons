/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Stochastic neuron module.
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module Neuron #(N)
(
	input wire iClock,
	input logic iRandomSource,
	input logic [N:1] iSynapses,
	output logic oActivation,
	output logic oDerivative
);

// -- Weighted sum.
logic SumOfSynapses;
StochasticGenericAdder #(N) adder(.iClock, .iRandomSource, .iStreams(iSynapses), .oStream(SumOfSynapses));

// -- Activation function.
StochasticTanh activation(.iClock, .iRandomSource, .iStream(SumOfSynapses), .oActivation, .oDerivative);

endmodule

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
//module AdaptableNeuron #(N)
//(
//	input wire iReset,
//	input wire iClock,
//	input wire iLearn,
//	input logic [N:1] iRandomSources,
//	input logic [2:0] iWeightGeneratorSeeds [N:1],
//	input logic [7:0] iInitialWeights [N:1],
//	input logic [N:1] iStreams,
//	output logic [N:1] oLocalFeedbacks,
//	input logic iLocalError,
//	output logic oActivation,
//	output logic oDerivative
//);
//
//
//SynapseWeight weights [3:1] (.iReset, .iClock, .iAdjust(Training), .iGeneratorSeed(LUTADDR),
//	.iInitialWeight(InitialWeights), .iAdjustedWeight(WeightUpdates), .oWeight(SynapseWeights), .oAdjusted(WeightReady));
//
//
//AdaptableSynapse syn1 (.iRandomSource(RandomStreams[31]), .iWeight(SynapseWeights[1]), .oAdjustedWeight(WeightUpdates[1]), .iStream(NeuronInputs[0]), .oStream(SynapseOutputs[1]), .iLocalError(Gradient), .oLocalFeedback());
//AdaptableSynapse syn2 (.iRandomSource(RandomStreams[30]), .iWeight(SynapseWeights[2]), .oAdjustedWeight(WeightUpdates[2]), .iStream(NeuronInputs[1]), .oStream(SynapseOutputs[2]), .iLocalError(Gradient), .oLocalFeedback());
//AdaptableSynapse syn3 (.iRandomSource(RandomStreams[29]), .iWeight(SynapseWeights[3]), .oAdjustedWeight(WeightUpdates[3]), .iStream(1'b1), .oStream(SynapseOutputs[3]), .iLocalError(Gradient), .oLocalFeedback());
//
///*	input wire iClock,
//	input logic iRandomSource,
//	input logic [N:1] iSynapses,
//	output logic oActivation,
//	output logic oDerivative*/
//
//Neuron #(3) neu1 (.iClock, .iRandomSource(RandomStreams[28]), .iSynapses(SynapseOutputs), .oActivation(NeuronOutput), .oDerivative(NeuronDerivative));
//
//
//// -- Weighted sum.
//logic SumOfSynapses;
//StochasticGenericAdder #(N) adder(.iClock, .iRandomSource, .iStreams(iSynapses), .oStream(SumOfSynapses));
//
//// -- Activation function.
//StochasticTanh activation(.iClock, .iRandomSource, .iStream(SumOfSynapses), .oActivation, .oDerivative);
//
//
//
//endmodule
