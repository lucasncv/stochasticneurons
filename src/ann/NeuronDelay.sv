/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* A delay for a neuron.
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module NeuronDelay(RESET, CLOCK, LUTADDRESS, INPUT, OUTPUT, STORE);

// -- The inputs.
input wire RESET;
input wire CLOCK;
input wire [2:0] LUTADDRESS;
input wire INPUT;
input wire STORE;

// -- The outputs.
output logic OUTPUT;

// -- Accumulator.
wire [8:0] CurrentBinary;
BitStreamAccumulator #(8) acc(RESET, CLOCK, INPUT, CurrentBinary, 1'bx);

// -- Rounder.
wire [7:0] RoundedBinary;
BinaryRounder #(8) rounder(CurrentBinary, RoundedBinary);

// -- Memory.
logic [7:0] StoredBinary;

always_ff @(posedge CLOCK)
	StoredBinary <= STORE ? RoundedBinary : StoredBinary;
	
// -- Generation.
assign OUTPUT = StoredBinary[LUTADDRESS];

endmodule
