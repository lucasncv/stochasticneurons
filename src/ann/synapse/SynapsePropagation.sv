/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module SynapsePropagation #(M, N)
(
	input wire iClock,
	input logic iRandomSource,
	input logic iActivations [M],
	input logic iWeights [N][M],
	input logic iBiases [N],
	output logic oNetInputs [N]
);

// -- Random source delays.
logic RandomSources [N];

DelayChain #(.WIDTH(1), .N(N)) delays
(
	.iClock,
	.iSignal(iRandomSource),
	.oSignal(RandomSources)
);

generate genvar n, m;
	for (n = 0; n < N; n++) begin : ForEachN
	
		// -- Components of the net input.
		wire [M:0] Terms;

		// -- Multiply inputs by weights.
		for (m = 0; m < M; m++) begin : ForEachM
			assign Terms[m] = ~(iActivations[m] ^ iWeights[n][m]);
		end
		
		// -- The bias is one of the terms.
		assign Terms[M] = iBiases[n];

		// -- Sum all multiplied inputs and biases.
		MarkovAdder #(M + 1) adder
		(
			.iClock,
			.iRandomSource(RandomSources[n]),
			.iStreams(Terms),
			.oStream(oNetInputs[n])
		);
		
	end
endgenerate

endmodule
