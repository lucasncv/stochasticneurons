/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module Synapses #(M, N)
(
	// Control
	input wire iClock,
	input logic iRandomSource,
	
	// Weights
	input logic iWeights [N * M + N],
	output logic oDeltaWeights [N * M + N],
	
	// Layers
	LayerRightSide.synapses PreviousLayer,
	LayerLeftSide.synapses NextLayer
);

// -- Weight stuff.
wire WeightMatrix [N][M];
wire Biases [N];

wire DeltaWeightMatrix [N][M];
wire DeltaBiases [N];

// -- Calculate net inputs.
SynapsePropagation #(M, N) calculateInputs
(
	.iClock,
	.iRandomSource,
	.iActivations(PreviousLayer.Activations),
	.iWeights(WeightMatrix),
	.iBiases(Biases),
	.oNetInputs(NextLayer.NetInputs)
);

// -- Calculate net errors.
SynapseBackPropagation #(M, N) calculateErrors
(
	.iClock,
	.iRandomSource,
	.iLocalErrors(NextLayer.LocalErrors),
	.iWeights(WeightMatrix),
	.oNetErrors(PreviousLayer.NetErrors)
);

// -- Calculate delta weights.
SynapseAdaptation #(M, N) calculateDeltas
(
	.iActivations(PreviousLayer.Activations),
	.iLocalErrors(NextLayer.LocalErrors),
	.oDeltaWeights(DeltaWeightMatrix),
	.oDeltaBiases(DeltaBiases)
);

// -- Manipulate weight storage.
WeightVectorToMatrix #(M, N) wv2m
(
	.iVector(iWeights),
	.oMatrix(WeightMatrix),
	.oBiases(Biases)
);

WeightMatrixToVector #(M, N) wm2v
(
	.iMatrix(DeltaWeightMatrix),
	.iBiases(DeltaBiases),
	.oVector(oDeltaWeights)
);

endmodule

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
interface LayerRightSide #(M);

// -- The activations of each neuron.
logic Activations [M];

// -- The backpropagated errors for each neuron.
logic NetErrors [M];

// -- Port directions.
modport layer (input NetErrors, output Activations);
modport synapses (output NetErrors, input Activations);
modport probe (input Activations);

endinterface

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
interface LayerLeftSide #(N);

// -- The action potentials of each neuron.
logic NetInputs [N];

// -- The local errors of each neuron.
logic LocalErrors [N];

// -- Port directions.
modport layer (input NetInputs, output LocalErrors);
modport synapses (output NetInputs, input LocalErrors);
modport probe (input NetInputs);

endinterface
