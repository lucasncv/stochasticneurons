/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module SynapseAdaptation #(M, N)
(
	input logic iActivations [M],
	input logic iLocalErrors [N],
	output logic oDeltaWeights [N][M],
	output logic oDeltaBiases [N]
);

generate genvar n, m;
	for (n = 0; n < N; n++) begin : ForEachN
		
		// -- Update biases.
		assign oDeltaBiases[n] = iLocalErrors[n];
		
		// -- Update weights.
		for (m = 0; m < M; m++) begin : ForEachM
			assign oDeltaWeights[n][m] = ~(iActivations[m] ^ iLocalErrors[n]);
		end
		
	end
endgenerate

endmodule
