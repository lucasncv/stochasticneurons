/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module SynapseBackPropagation #(M, N)
(
	input wire iClock,
	input logic iRandomSource,
	input logic iLocalErrors [N],
	input logic iWeights [N][M],
	output logic oNetErrors [M]
);

// -- Random source delays.
logic RandomSources [M];

DelayChain #(.WIDTH(1), .N(M)) delays
(
	.iClock,
	.iSignal(iRandomSource),
	.oSignal(RandomSources)
);

generate genvar n, m;
	for (m = 0; m < M; m++) begin : ForEachM
	
		// -- Components of the net error.
		wire [N-1:0] Terms;

		// -- Multiply errors by weights.
		for (n = 0; n < N; n++) begin : ForEachN
			assign Terms[n] = ~(iLocalErrors[n] ^ iWeights[n][m]);
		end

		// -- Sum all multiplied errors.
		MarkovAdder #(N) adder
		(
			.iClock,
			.iRandomSource(RandomSources[m]),
			.iStreams(Terms),
			.oStream(oNetErrors[m])
		);
		
	end
endgenerate

endmodule
