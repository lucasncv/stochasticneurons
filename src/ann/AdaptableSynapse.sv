/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Adaptable stochastic synapse module.
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module AdaptableSynapse
(
	input logic iRandomSource,
	input logic iWeight,
	output logic oAdjustedWeight,
	input logic iStream,
	output logic oStream,
	input logic iLocalError,
	output logic oLocalFeedback
);

// -- Propagation.
assign oStream = ~(iStream ^ iWeight);

// -- Back-propagation.
assign oLocalFeedback = ~(iLocalError ^ iWeight);

// -- Learning.
wire Delta = ~(iStream ^ iLocalError);
StochasticAdder adder (.iA(Delta), .iB(iWeight), .iRandomSource, .oStream(oAdjustedWeight));

endmodule

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Adjustable synapse weight.
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module SynapseWeight
(
	input wire iReset,
	input wire iClock,
	input wire iAdjust,
	input logic [2:0] iGeneratorSeed,
	input logic [7:0] iInitialWeight,
	input logic iAdjustedWeight,
	output logic oWeight,
	output logic oAdjusted
);

// -- Current weight.
logic [7:0] BinaryWeight;
assign oWeight = BinaryWeight[iGeneratorSeed];

// -- Update Weight.
logic [7:0] BinaryHalfAdjustedWeight;

ProbabilityEstimator #(8) est (iReset | ~iAdjust, iClock, iAdjustedWeight, BinaryHalfAdjustedWeight, oAdjusted);

wire [7:0] BinaryAdjustedWeight = BinaryHalfAdjustedWeight[7] == BinaryHalfAdjustedWeight[6] ? { 8 { BinaryHalfAdjustedWeight[7] } } :
																																{ ~BinaryHalfAdjustedWeight[6], BinaryHalfAdjustedWeight[5:0], 1'b0 };

always_ff @(posedge iClock)
	if (iReset)
		BinaryWeight <= iInitialWeight;
	else if (oAdjusted)
		BinaryWeight <= BinaryAdjustedWeight;

endmodule
