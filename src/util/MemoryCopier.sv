/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Copies data between two memory blocks.
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module MemoryCopier #(WIDTH, N)
(
	input wire iClock,
	input wire iReset,
	input logic iDirection,
	output logic [(N > 1 ? $clog2(N) : 1):1] oAddress,
	inout tri [WIDTH:1] ioDataA,
	inout tri [WIDTH:1] ioDataB,
	output logic oDone
);

// -- Logic.
always @(posedge iClock)
	oAddress <= iReset || oDone ? '0 : oAddress + 1;
	
assign oDone = !iReset && oAddress == N - 1;

// -- Make connections.
assign ioDataA = iDirection && !iReset ? ioDataB : 'z;
assign ioDataB = iDirection || iReset ? 'z : ioDataA;

endmodule
