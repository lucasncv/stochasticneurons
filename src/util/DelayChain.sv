/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Delays a signal multiple times.
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module DelayChain #(WIDTH, N)
(
	input wire iClock,
	input logic [WIDTH:1] iSignal,
	output logic [WIDTH:1] oSignal [N]
);

// -- First element has no delay.
assign oSignal[0] = iSignal;

// -- Next delays.
generate if (N > 1)
	always @(posedge iClock)
		oSignal[1:N-1] <= oSignal[0:N-2];
endgenerate

endmodule
