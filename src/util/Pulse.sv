module Pulse
(
	input wire iClock,
	input wire iSignal,
	output wire oHigh,
	output wire oLow
);

logic FF = '0;

always_ff @(posedge iClock)
	FF <= iSignal;
	
assign oHigh = iSignal && !FF;
assign oLow = !iSignal && FF;

endmodule
