/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Interfaces an 8bit memory of N elements.
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
interface IByteMemory #(N)();

// -- Address width.
localparam W = (N > 1 ? $clog2(N) : 1) - 1;

// -- Writing or reading?
logic WriteEnable;

// -- Data being read.
wire [7:0] ReadData;

// -- Data to be written.
wire [7:0] WriteData;

// -- The current address.
logic [W:0] Address = '0;

endinterface
