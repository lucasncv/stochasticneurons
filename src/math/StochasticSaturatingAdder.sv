/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Stochastic saturating adder. (N Bipolar Inputs)
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module StochasticSaturatingAdder(CLOCK, INPUTS, OUTPUT);

// -- The number of inputs.
parameter N = 3;

// -- The inputs.
input logic CLOCK;
input logic [1:N] INPUTS;

// -- The outputs.
output logic OUTPUT;

// -- Single input.
generate
if (N == 1) begin

	assign OUTPUT = INPUTS[1];
	
end
endgenerate

// -- Multiple inputs.
generate
if (N > 1) begin

	// -- Get an odd number of inputs.
	localparam oddN = N + 1 - (N % 2);
	wire [1:oddN] Inputs;
	
	if (N % 2) begin
	
		assign Inputs = INPUTS;
	
	end else begin
	
		logic ZeroInput;
		
		always_ff @(posedge CLOCK)
			ZeroInput <= ~ZeroInput;
			
		assign Inputs = { INPUTS, ZeroInput };
	
	end

	// -- Generates a chain of logic elements.
	localparam StageCount = 1 + (oddN - 3) / 2;
	wire [1:StageCount] Stages;
	
	assign Stages[1] = LogicElement(Inputs[1:3]);
	genvar i;
	
	for (i = 2; i <= StageCount; i = i + 1) begin : FOR
		assign Stages[i] = LogicElement({ Inputs[(i*2)+:2], Stages[i - 1] });
	end
	
	// -- The output signal.
	assign OUTPUT = Stages[StageCount];
	
end
endgenerate

// -- A single adder of three inputs.
function LogicElement(input [2:0] data);
	LogicElement = data[2] ? |data[1:0] : &data[1:0];
endfunction

endmodule
