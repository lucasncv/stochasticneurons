/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Stochastic hyperbolic tangent and derivative (/2).
* Author: Lucas Neves Carvalho (100% TESTED!)
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module StochasticTanh
(
	input wire iClock,
	input logic iRandomSource,
	input logic iStream,
	output logic oActivation,
	output logic oDerivative
);

// -- Register.
logic [2:0] State = 3'b0;

always @(posedge iClock)
case ({ iStream, State})
	4'b0000: State <= 3'b000;
	4'b0001: State <= 3'b000;
	4'b0010: State <= 3'b001;
	4'b0011: State <= 3'b010;
	4'b0100: State <= 3'b011;
	4'b0101: State <= 3'b100;
	4'b0110: State <= 3'b101;
	4'b0111: State <= 3'b110;
	
	4'b1000: State <= 3'b001;
	4'b1001: State <= 3'b010;
	4'b1010: State <= 3'b011;
	4'b1011: State <= 3'b100;
	4'b1100: State <= 3'b101;
	4'b1101: State <= 3'b110;
	4'b1110: State <= 3'b111;
	4'b1111: State <= 3'b111;
endcase

// -- Output bit-stream.
assign oActivation = State[2];

// -- Derivative.
wire DerivativeMinusOne = State[2] ^ State[1];	
StochasticAdder adder(.iA(1'b1), .iB(DerivativeMinusOne), .iRandomSource, .oStream(oDerivative));

endmodule
