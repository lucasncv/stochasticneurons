/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Markov Adder. (N Bipolar Inputs)
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module MarkovAdder #(N)
(
	input wire iClock,
	input logic iRandomSource,
	input logic [N:1] iStreams,
	output logic oStream
);

generate 

// -- Optimization for single input.
if (N == 1)
	assign oStream = iStreams[1];
	
// -- Optimization for two inputs.
else if (N == 2)
	assign oStream = iRandomSource ? iStreams[1] : iStreams[2];

// -- General case.
else begin
	// -- Markov shift register.
	logic [N:1] States = { '0, 1'b1 };

	// -- Transition logic.
	always_ff @(posedge iClock)
		if (iRandomSource)
			States <= { States[N-1:1], States[N] };
		else
			States <= { States[1], States[N:2] };

	// -- Output.
	assign oStream = |(States & iStreams);
end

endgenerate

endmodule
