/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Rounds a binary probability.
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module BinaryRounder(INPUT, OUTPUT);

// -- The target probability width.
parameter WIDTH = 8;

// -- The inputs.
input wire [WIDTH:0] INPUT;

// -- The outputs.
output logic [WIDTH-1:0] OUTPUT;

// -- Logic.
assign OUTPUT = INPUT[WIDTH] ? { WIDTH { 1'b1 } } : INPUT[WIDTH-1:0];

endmodule
