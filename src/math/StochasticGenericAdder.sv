/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Stochastic Generic Adder. (N Bipolar Inputs)
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module StochasticGenericAdder #(N = 3)
(
	input wire iClock,
	input logic iRandomSource,
	input logic [N-1:0] iStreams,
	output logic oStream
);

// -- Input selector.
localparam NW = N > 1 ? $clog2(N) : 1;

logic [NW-1:0] Select = 'b0;
wire [NW-1:0] NextSelect = Select + 1;

always_ff @(posedge iClock)
	Select <= NextSelect == N ? 0 : NextSelect;
	
// -- Result.
logic LastResult;
wire Result = iStreams[Select];

always_ff @(posedge iClock)
	LastResult <= Result;
	
// -- Multiplexer.
assign oStream = iRandomSource ? Result : LastResult;

endmodule

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module StochasticArrayAdder #(N)
(
	input wire iClock,
	input logic iRandomSource,
	input logic iStreams [N],
	output logic oStream
);

// -- Input selector.
localparam NW = (N > 1 ? $clog2(N) : 1) - 1;

logic [NW:0] Select = '0;
wire [NW:0] NextSelect = Select + 1;

always_ff @(posedge iClock)
	Select <= NextSelect == N ? '0 : NextSelect;
	
// -- Result.
logic LastResult;
wire Result = iStreams[Select];

always_ff @(posedge iClock)
	LastResult <= Result;
	
// -- Multiplexer.
assign oStream = iRandomSource ? Result : LastResult;

endmodule
