/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Stochastic Adder. (Two Inputs)
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module StochasticAdder
(
	input logic iA,
	input logic iB,
	input logic iRandomSource,
	output logic oStream
);

// -- Multiplexer.
assign oStream = iRandomSource ? iA : iB;

endmodule
