/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Top Module
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module TopLearningXOR
(
	input wire CLOCK_50,
	input wire [3:0] KEY,
	input wire [17:0] SW,
	output wire [17:0] LEDR,
	output wire [8:0] LEDG
);

// -- Weights.
localparam WEIGHT_COUNT = 6;

wire Weights [WEIGHT_COUNT];
wire DeltaWeights [WEIGHT_COUNT];

WeightBank #(.N(WEIGHT_COUNT)) weightssss
(
	.iClock,
	.iGenerationSeed(LUTADDR[1]),
	.iDeltaWeights(DeltaWeights),
	.oWeights(Weights),
	.iAddress(),
	.iWriteEnable('0),
	.ioData()
);

Synapses #(.M(2), .N(2)) synap
(
	.iClock,
	.iWeights(Weights),
	.oDeltaWeights(DeltaWeights)
);

InputLayer #(2) layer0
(
	.iStreams('{ SW[1], SW[0] }),
	.FirstLayer(synap)
);

OutputLayer #(2) layer1
(
	.iClock,
	.iRandomSource(RandomStreams[0]),
	.PreviousLayer(synap),
	.iExpectedOutputs('{ SW[14], SW[13] }),
	.oActualOutputs('{ LEDG[8], LEDG[7] })
);

// -- Control.
wire iClock = CLOCK_50;
wire iReset = ~KEY[0];
wire iLearn = ~KEY[1];

// -- Linear Feedback Shift Register.
bit [31:0] RandomStreams;
LFSR32 lfsr(iClock, RandomStreams);

// -- Address generators.
bit [2:0] AddressBitStreams;
AddressBitStreamsGenerator addr(iClock, RandomStreams[17:0], AddressBitStreams);

// -- Address delays.
wire [2:0] LUTADDR [1:3];
SignalDelays #(3, 3) delays(iClock, AddressBitStreams, LUTADDR);

// -- Training data.
wire [3:0][1:0] InputSets = { 2'b11, 2'b10, 2'b01, 2'b00 };
wire [3:0] OutputSets = SW[14:11];

// -- Training control.
logic [8:0] TrainingTimer;
wire [3:1] WeightReady;

wire [1:0] CurrentSet = TrainingTimer[1:0];
wire Training = iLearn && !TrainingTimer[8];

always_ff @(posedge iClock)
	if (iReset)
		TrainingTimer <= 9'b0;
	else if (iLearn && WeightReady[1])
		TrainingTimer <= TrainingTimer + 1'b1;

// -- Inputs and Outputs.
wire [1:0] NeuronInputs = iLearn ? InputSets[CurrentSet] : SW[1:0];
wire ExpectedOutput = OutputSets[CurrentSet];

// -- Synapses.
wire [3:1] SynapseOutputs;
wire NeuronOutput, NeuronDerivative, Gradient;
wire NetworkPerformance;
wire [3:1] WeightUpdates;
wire [3:1] SynapseWeights;
/*	input wire iReset,
	input wire iClock,
	input wire iAdjust,
	input logic [2:0] iGeneratorSeed,
	input logic [7:0] iInitialWeight,
	input logic iAdjustedWeight,
	output logic oWeight,
	output logic oAdjusted*/
	
	/*	input logic iRandomSource,
	input logic iWeight,
	output logic oAdjustedWeight,
	input logic iStream,
	output logic oStream,
	input logic iLocalError,
	output logic oLocalFeedback*/
	
wire [3:1][7:0] InitialWeights = RandomStreams[23:0];//{ 8'd145, 8'd208, 8'd51 };

SynapseWeight weights [3:1] (.iReset, .iClock, .iAdjust(Training), .iGeneratorSeed(LUTADDR),
	.iInitialWeight(InitialWeights), .iAdjustedWeight(WeightUpdates), .oWeight(SynapseWeights), .oAdjusted(WeightReady));

AdaptableSynapse syn1 (.iRandomSource(RandomStreams[31]), .iWeight(SynapseWeights[1]), .oAdjustedWeight(WeightUpdates[1]), .iStream(NeuronInputs[0]), .oStream(SynapseOutputs[1]), .iLocalError(Gradient), .oLocalFeedback());
AdaptableSynapse syn2 (.iRandomSource(RandomStreams[30]), .iWeight(SynapseWeights[2]), .oAdjustedWeight(WeightUpdates[2]), .iStream(NeuronInputs[1]), .oStream(SynapseOutputs[2]), .iLocalError(Gradient), .oLocalFeedback());
AdaptableSynapse syn3 (.iRandomSource(RandomStreams[29]), .iWeight(SynapseWeights[3]), .oAdjustedWeight(WeightUpdates[3]), .iStream(1'b1), .oStream(SynapseOutputs[3]), .iLocalError(Gradient), .oLocalFeedback());

/*	input wire iClock,
	input logic iRandomSource,
	input logic [N:1] iSynapses,
	output logic oActivation,
	output logic oDerivative*/

Neuron #(3) neu1 (.iClock, .iRandomSource(RandomStreams[28]), .iSynapses(SynapseOutputs), .oActivation(NeuronOutput), .oDerivative(NeuronDerivative));


/*	input logic iRandomSource,
	input logic iDerivative,
	input logic iActualOutput,
	input logic iExpectedOutput,
	output logic oLocalError,
	output logic oPerformance*/

OutputLayerTrainer tra1 (.iRandomSource(RandomStreams[27]), .iDerivative(NeuronDerivative), .iActualOutput(NeuronOutput), .iExpectedOutput(ExpectedOutput), .oLocalError(Gradient), .oPerformance(NetworkPerformance));

// -- Performance.
(*noprune*) logic [7:0] EstimatedPerformance;
logic PerfReady;

ProbabilityEstimator #(8) estperf (!Training, iClock, NetworkPerformance, EstimatedPerformance, PerfReady);
TrainingHistory perfmem (.address(TrainingTimer), .clock(iClock), .data(EstimatedPerformance), .wren(PerfReady), .q());

// -- Accumulator input.
//logic EstimatorInput;

//always case (SW[17:15])
//	default: EstimatorInput = NeuronOutput;
//endcase

// -- Accumulators.
(*noprune*) logic [7:0] EstimatedProbability;
(*noprune*) logic AccReady;

ProbabilityEstimator #(8) est (iReset, iClock, NeuronOutput, EstimatedProbability, AccReady);

// -- LED feedback.
ProbabilityLEDFeedback leds(EstimatedProbability[7:4], LEDR);


endmodule
