/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Test of a temporal XOR neural network (Elman)
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module TopTemporalXOR
(
	CLOCK_50,
	KEY, SW, LEDG, LEDR
);

// -- The inputs.
input wire CLOCK_50;

input wire [3:0] KEY;
input wire [17:0] SW;

// -- The outputs.
output wire [17:0] LEDR;
output wire [8:0] LEDG;

// -- Control.
wire CLOCK = CLOCK_50;
wire RESET = ~KEY[0];
wire STORE = ~KEY[1];


// -- Linear Feedback Shift Register.
bit [31:0] RandomStreams;
LFSR32 lfsr(CLOCK, RandomStreams);


// -- Address generators.
bit [2:0] AddressBitStreams;
AddressBitStreamsGenerator addr(CLOCK, RandomStreams[17:0], AddressBitStreams);

// -- Address delays.
wire [2:0] LUTADDR [1:6];
SignalDelays #(3, 6) delays(CLOCK, AddressBitStreams, LUTADDR);


// -- NN Input layer.
wire InputStream;
BitStreamGenerator #(4) inputGen (CLOCK, SW[3:0], RandomStreams[27:24], { InputStream });


// -- NN Hidden layer.
logic [2:0] HiddenLayer;
CompleteNeuron #(5) neuron1(CLOCK, '{ 8'hFF, 8'h80, 8'h80, 8'hFF, 8'hFF }, LUTADDR[2:6], { InputStream, ContextLayer[0], ContextLayer[1], ContextLayer[2], 1'b1 }, HiddenLayer[0], 1'bx);
CompleteNeuron #(5) neuron2(CLOCK, '{ 8'h00, 8'h80, 8'h80, 8'h00, 8'hFF }, LUTADDR[2:6], { InputStream, ContextLayer[0], ContextLayer[1], ContextLayer[2], 1'b1 }, HiddenLayer[1], 1'bx);
CompleteNeuron #(5) neuron3(CLOCK, '{ 8'hFF, 8'h80, 8'h80, 8'h80, 8'h80 }, LUTADDR[2:6], { InputStream, ContextLayer[0], ContextLayer[1], ContextLayer[2], 1'b1 }, HiddenLayer[2], 1'bx);


// -- NN Context layer.
logic [2:0] ContextLayer;
NeuronDelay contextDelays [1:3] (RESET, CLOCK, LUTADDR[1], HiddenLayer, ContextLayer, STORE);


// -- NN Output layer.
logic NeuralOutput;
CompleteNeuron #(4) neuronOut(CLOCK, '{ 8'hFF, 8'hFF, 8'h80, 8'h2D }, LUTADDR[3:6], { HiddenLayer[0], HiddenLayer[1], HiddenLayer[2], 1'b1 }, NeuralOutput, 1'bx);


// -- Accumulator input.
reg AccumulatorInput;

always case (SW[17:15])
	3'b000: AccumulatorInput = InputStream;
	3'b001: AccumulatorInput = ContextLayer[0];
	3'b010: AccumulatorInput = ContextLayer[1];
	3'b011: AccumulatorInput = ContextLayer[2];
	default: AccumulatorInput = NeuralOutput;
endcase

// -- Accumulators.
wire [8:0] AccumulatorOutput;
BitStreamAccumulator #(8) acc8(RESET, CLOCK, AccumulatorInput, AccumulatorOutput, 1'bx);

// -- 8-bit Probability.
(*noprune*) logic [7:0] Probability8bit;

always_ff @(posedge CLOCK)
	Probability8bit <= AccumulatorOutput[8] ? 8'hFF : AccumulatorOutput[7:0];

// -- LED feedback.
ProbabilityLEDFeedback leds(Probability8bit[7:4], LEDR);


endmodule
