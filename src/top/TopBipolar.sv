/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Top Module
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module TopBipolar
(
	CLOCK_50,
	KEY, SW, LEDG, LEDR
);

// -- The inputs.
input wire CLOCK_50;

input wire [3:0] KEY;
input wire [17:0] SW;

// -- The outputs.
output wire [17:0] LEDR;
output wire [8:0] LEDG;

// -- Control.
wire CLOCK = CLOCK_50;
wire RESET = ~KEY[0];

// -- Linear Feedback Shift Register.
bit [31:0] RandomStreams;
LFSR32 lfsr(CLOCK, RandomStreams);

// -- SNG.
wire [2:0] Streams;
BitStreamGenerator #(4) gen [2:0] (CLOCK, SW[11:0], RandomStreams[11:0], Streams);

// -- Operations.
wire Multiplication = ~(Streams[0] ^ Streams[1]);

wire Sum2;
StochasticAdder adder(CLOCK, Streams[0], Streams[1], Sum2);

wire Sat3;
StochasticSaturatingAdder #(9) saturating(CLOCK, { Streams, Streams, Streams }, Sat3);

// -- Accumulator input.
logic AccumulatorInput;

always case (SW[17:15])
	3'b000: AccumulatorInput = Streams[0];
	3'b001: AccumulatorInput = Streams[1];
	3'b010: AccumulatorInput = Streams[2];
	3'b011: AccumulatorInput = Multiplication;
	3'b100: AccumulatorInput = Sum2;
	3'b101: AccumulatorInput = Sat3;
	3'b110: AccumulatorInput = RandomStreams[0];
	3'b111: AccumulatorInput = RandomStreams[0];
endcase

// -- Accumulators.
wire [8:0] AccumulatorOutput;
BitStreamAccumulator #(8) acc8(RESET, CLOCK, AccumulatorInput, AccumulatorOutput, 1'bx);

// -- 8-bit Probability.
(*noprune*) logic [7:0] Probability8bit;

always_ff @(posedge CLOCK)
	Probability8bit <= AccumulatorOutput[7:0];

// -- Round probability.
logic [3:0] RoundedProbability;

always_ff @(posedge CLOCK)
	RoundedProbability <= AccumulatorOutput[7:4] + AccumulatorOutput[3];

// -- LED feedback.
ProbabilityLEDFeedback leds(RoundedProbability, LEDR);

endmodule
