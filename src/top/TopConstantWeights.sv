/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Top Module
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module TopConstantWeights
(
	input wire CLOCK_50,
	input wire [3:0] KEY,
	input wire [17:0] SW,
	output wire [17:0] LEDR,
	output wire [8:0] LEDG
);

// -- Inputs and outputs.
wire NNInputs [2] = '{ SW[1], SW[0] };
wire NNOutputs [1];

// -- Control.
wire iClock = CLOCK_50;

// -- ANN.
ConstantNeuralNetwork ANN
(
	.iClock,
	.iInputs(NNInputs),
	.oOutputs(NNOutputs)
);

// -- Output.
logic [7:0] EstimatedProbability;
ProbabilityEstimator #(8) est ('0, iClock, NNOutputs[0], EstimatedProbability);

// -- LED feedback.
ProbabilityLEDFeedback leds(EstimatedProbability[7:4], LEDR);

endmodule

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module ConstantNeuralNetwork
(
	input wire iClock,
	input logic iInputs [2],
	output logic oOutputs [1]
);

// -- Generic parameters.
localparam InputSize = 2;
localparam HiddenSize = 2;
localparam OutputSize = 1;

// -- Derived parameters.
localparam InputToHiddenCount = (InputSize + 1) * HiddenSize;
localparam HiddenToOutputCount = (HiddenSize + 1) * OutputSize;

// -- More derived parameters.
localparam I = InputSize;
localparam O = OutputSize;
localparam WN = InputToHiddenCount + HiddenToOutputCount;

// -- Linear Feedback Shift Register.
bit [31:0] RandomStreams;
LFSR32 lfsr(iClock, RandomStreams);

// -- Address generators.
bit [2:0] WeightSeeds;
AddressBitStreamsGenerator addressGenerator (iClock, RandomStreams[16:0], WeightSeeds);

// -- Weights.
wire Weights [WN];

ConstantWeightUnit #(.N(WN)) weightUnit
(
	.iClock,
	.iGenerationSeed(WeightSeeds),
	.iLearningFactor(),
	.iDeltaWeights(),
	.oWeights(Weights),
	.Control()
);

// -- Input Layer.
LayerRightSide #(InputSize) inputLayerRS();

InputLayer #(InputSize) inputLayer
(
	.iStreams(iInputs),
	.NextLayer(inputLayerRS)
);

// -- Hidden Layer.
LayerLeftSide #(HiddenSize) hiddenLayerLS();
LayerRightSide #(HiddenSize) hiddenLayerRS();

HiddenLayer #(HiddenSize) hiddenLayer
(
	.iClock,
	.iRandomSource(),
	.PreviousLayer(hiddenLayerLS),
	.NextLayer(hiddenLayerRS)
);

// -- Output Layer.
LayerLeftSide #(OutputSize) outputLayerLS();

OutputLayer #(OutputSize) outputLayer
(
	.iClock,
	.iRandomSource(),
	.PreviousLayer(outputLayerLS),
	.iExpectedOutputs(),
	.oActualOutputs(oOutputs),
	.oPerformance()
);

// -- Synapses.
Synapses #(.M(InputSize), .N(HiddenSize)) synapses0
(
	.iClock,
	.iRandomSource(RandomStreams[23]),
	.iWeights(Weights[0+:InputToHiddenCount]),
	.oDeltaWeights(),
	.PreviousLayer(inputLayerRS),
	.NextLayer(hiddenLayerLS)
);

Synapses #(.M(HiddenSize), .N(OutputSize)) synapses1
(
	.iClock,
	.iRandomSource(RandomStreams[29]),
	.iWeights(Weights[InputToHiddenCount+:HiddenToOutputCount]),
	.oDeltaWeights(),
	.PreviousLayer(hiddenLayerRS),
	.NextLayer(outputLayerLS)
);

endmodule

