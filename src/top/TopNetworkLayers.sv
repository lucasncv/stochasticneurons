/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Top Module
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module TopNetworkLayers
(
	input wire CLOCK_50,
	input wire [3:0] KEY,
	input wire [17:0] SW,
	output wire [17:0] LEDR,
	output wire [8:0] LEDG
);

// -- Paramters.
localparam I = 2;
localparam O = 1;

// -- Control.
wire iClock = CLOCK_50;
wire iExecute, iLearn;
wire iTestMode = SW[12];

Debouncer resetDebouncer (.iClock, .iBouncy(~KEY[0]), .oPulse(iExecute), .oState());
Debouncer learnDebouncer (.iClock, .iBouncy(~KEY[1]), .oPulse(), .oState(iLearn));

// -- Linear Feedback Shift Register.
bit [31:0] RandomStreams;
LFSR32 lfsr(iClock, RandomStreams);

// -- Learning Factor.
wire LearningFactor;
BitStreamGenerator #(3) gen(iClock, SW[10:7], RandomStreams[31:29], LearningFactor);

// -- Cycle control.
wire CycleStart, CycleEnd, ExecuteTraining;

wire TCReady, TCNowReady;
wire NNBusy, NNStartedWork, NNFinishedWork;

wire NetworkIsWarmedUp;
wire AccumulatedOutputs;

Pulse pulse0 (.iClock, .iSignal(TCReady), .oHigh(TCNowReady), .oLow());
Pulse pulse1 (.iClock, .iSignal(NNBusy), .oHigh(NNStartedWork), .oLow(NNFinishedWork));

assign CycleStart = iTestMode ? NetworkIsWarmedUp : NNStartedWork;
assign CycleEnd = iTestMode ? AccumulatedOutputs : NNFinishedWork;
assign ExecuteTraining = !iTestMode && NetworkIsWarmedUp;

// -- Training.
wire InputSet [I];
wire OutputSet [O];
wire Performance [O];

TrainingControl #(.I(I), .O(O)) training
(
	.iClock,
	.iReset(~iLearn),
	.iNext(CycleEnd),
	.oReady(TCReady),
	.oInputSet(InputSet),
	.oOutputSet(OutputSet)
);

// -- Warm up between sets.
WarmUpTimer warmUp
(
	.iClock,
	.iReset(~iLearn),
	.iStartPulse(TCNowReady),
	.oEndPulse(NetworkIsWarmedUp)
);

// -- Inputs and outputs.
wire NNInputs [I];
wire NNOutputs [O];

generate genvar n;
	for (n = 0; n < I; n++) begin : ForEachInput
		assign NNInputs[n] = iLearn ? InputSet[n] : SW[n];
	end
endgenerate

// -- Network.
NeuralNetworkControl Control
(
	.iClock,
	.iOperation(iExecute ? SW[17:15] : { 2'b0, ExecuteTraining }),
	.oBusy(NNBusy)
);

//RecurrentNeuralNetwork ann
//NeuralNetwork ann
NeuralNetworkWithTwoHiddenLayers ann
(
	.iClock,
	.iLearningFactor(LearningFactor),
	.iInputs(NNInputs),
	.iExpectedOutputs(OutputSet),
	.oActualOutputs(NNOutputs),
	.oPerformance(Performance),
	.Control(Control)
);

// -- Output stuff.
logic [7:0] FirstBinaryOutput;

generate
	for (n = 0; n < O; n++) begin : ForEachOutput
	
		// -- Performance.
		ProbabilityEstimator #(8) estperf (CycleStart, iClock, Performance[n]);
	
		// -- Output.
		logic [7:0] EstimatedProbability;
		logic AccReady;

		ProbabilityEstimator #(8) est (CycleStart, iClock, NNOutputs[n], EstimatedProbability, AccReady);
		
		// -- First.
		if (n == 0) begin
			assign AccumulatedOutputs = AccReady;
			assign FirstBinaryOutput = EstimatedProbability;
		end
	end
endgenerate

// -- LED feedback.
ProbabilityLEDFeedback leds(FirstBinaryOutput[7:4], LEDR);
assign LEDG[0] = AccumulatedOutputs;

endmodule

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module WarmUpTimer
(
	input wire iClock,
	input wire iReset,
	input wire iStartPulse,
	output wire oEndPulse
);

logic [7:0] Timer = '0;
logic Enabled = '0;

always_ff @(posedge iClock)
	if (iReset || oEndPulse)
		Enabled <= 0;
	else if (iStartPulse)
		Enabled <= 1;
		
always_ff @(posedge iClock)
	Timer <= Enabled ? Timer + 1 : '0;
	
// -- End pulse.
assign oEndPulse = Timer[7];

endmodule
