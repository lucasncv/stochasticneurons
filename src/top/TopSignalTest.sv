/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Top Module
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module TopSignalTest
(
	input wire CLOCK_50,
	input wire [3:0] KEY,
	input wire [17:0] SW,
	output wire [17:0] LEDR,
	output wire [8:0] LEDG
);

// -- Slower clock.
logic CLOCK_25 = '0;

always_ff @(posedge CLOCK_50)
	CLOCK_25 <= ~CLOCK_25;

// -- Control.
wire iClock = CLOCK_25;
wire iReset, iLearn;

Debouncer resetDebouncer (.iClock, .iBouncy(~KEY[0]), .oPulse(), .oState(iReset));
Debouncer learnDebouncer (.iClock, .iBouncy(~KEY[1]), .oPulse(), .oState(iLearn));

// -- Linear Feedback Shift Register.
bit [31:0] RandomStreams;
LFSR32 lfsr(iClock, RandomStreams);

// -- Address generators.
bit [2:0] AddressBitStreams;
AddressBitStreamsGenerator addr(iClock, RandomStreams[16:0], AddressBitStreams);

// -- State machine.
typedef enum logic { PREPARE, ESTIMATE } StateCodes;

StateCodes State = PREPARE;
StateCodes NextState;
wire InTransition = State != NextState;

logic [4:0] StateTick = '0;

always_ff @(posedge iClock) begin
	StateTick <= InTransition ? '0 : StateTick + 1;
	State <= NextState;
end

// -- Chooses the next state.
always_comb begin
	NextState = State;
	
	case (State)
		PREPARE:
			if (iLearn && StateTick[4])
				NextState = ESTIMATE;
		ESTIMATE:
			if (!iLearn || ReadyToSample)
				NextState = PREPARE;
	endcase
	
end

// -- Next state logic.
always_ff @(posedge iClock)
	case (NextState)
		PREPARE:
			if (InTransition)
				InputCounter <= NextInput[7:0];
			else if (!iLearn)
				InputCounter <= '0;
	endcase

// -- Input
logic [7:0] InputCounter = '0;
wire [8:0] NextInput = InputCounter + 1;
wire [8:0] CorrectInput = InputCounter[7] ? NextInput : { '0, InputCounter };

wire InputStream, ActivationStream, DerivativeStream;

wire [3:0] InputStreams;
wire OutputStream;

assign InputStream = InputStreams[SW[3:2]];

// -- Input Methods.
BitStreamGenerator #(8) gen(iClock, CorrectInput, RandomStreams[7:0], InputStreams[0]);
WBG #(8) wbg256 (.iClock, .iProbability(CorrectInput), .iRandomSource(RandomStreams[7:0]), .oStream(InputStreams[1]));
WBG #(8) sng (.iClock, .iProbability(CorrectInput), .iRandomSource(RandomStreams[7:0]), .oStream(InputStreams[2]));
SMUX smux (.iClock, .iProbability(CorrectInput), .iSeed(AddressBitStreams), .oStream(InputStreams[3]));

// -- Activation and Derivative.
StochasticTanh activation
(
	.iClock,
	.iRandomSource(RandomStreams[17]),
	.iStream(InputStream),
	.oActivation(ActivationStream),
	.oDerivative(DerivativeStream)
);

// -- Delayed input.
wire InputDelays [8];

DelayChain #(1, 8) delay
(
	.iClock,
	.iSignal(InputStream),
	.oSignal(InputDelays)
);

wire DelayedInput = InputDelays[SW[6:4]];

// -- Power of 2.
wire PowerOf2 = ~(InputStream ^ DelayedInput);

// -- Output.
always_comb case (SW[1:0])
	2'b00: OutputStream <= ActivationStream;
	2'b01: OutputStream <= DerivativeStream;
	2'b10: OutputStream <= InputStream;
	2'b11: OutputStream <= PowerOf2;
endcase

// -- Estimation.
(*noprune*) logic [7:0] EstimatedSignal;
(*noprune*) logic Estimated;

ProbabilityEstimator #(8) estperf (State != ESTIMATE, iClock, OutputStream, EstimatedSignal, Estimated);

// -- Sampling.
(*noprune*) logic ReadyToSample;
Pulse pulse0 (.iClock, .iSignal(Estimated), .oHigh(ReadyToSample), .oLow());

endmodule
