/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Top Module
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module TopDoubleTest
(
	input wire CLOCK_50,
	input wire [3:0] KEY,
	input wire [17:0] SW,
	output wire [17:0] LEDR,
	output wire [8:0] LEDG
);

// -- Slower clock.
logic CLOCK_25 = '0;

always_ff @(posedge CLOCK_50)
	CLOCK_25 <= ~CLOCK_25;

// -- Control.
wire iClock = CLOCK_25;
wire iReset, iTest, StartTest;

Debouncer resetDebouncer (.iClock, .iBouncy(~KEY[0]), .oPulse(), .oState(iReset));
Debouncer testDebouncer (.iClock, .iBouncy(~KEY[1]), .oPulse(StartTest), .oState(iTest));

// -- Linear Feedback Shift Register.
bit [31:0] RandomStreams;
LFSR32 lfsr(iClock, RandomStreams);

// -- Address generators.
bit [2:0] AddressBitStreams;
AddressBitStreamsGenerator addr(iClock, RandomStreams[16:0], AddressBitStreams);


// -- Parameters for the test.
wire [7:0] DeltaAccumulator = SW[7:0];
wire [7:0] InitialAccumulator = SW[15:8];

(*noprune*) logic [7:0] Accumulator = '0;
wire AccumulatorStream = Accumulator[AddressBitStreams];

wire DeltaStream;
BitStreamGenerator #(8) gen0(iClock, { 1'b0, DeltaAccumulator }, RandomStreams[25:18], DeltaStream);

// -- Adjuster.
wire [7:0] NextAccumulator;
wire UpdateAccumulator;

//SingleWeightAdjuster adj
//(
//	.iClock,
//	.iEnable(iTest),
//	.iRandomSource(RandomStreams[31]),
//	.iDelta(DeltaStream),
//	.iWeight(AccumulatorStream),
//	.oWeight(NextAccumulator),
//	.oAdjusted(),//UpdateAccumulator),
//	.oDebug()
//);

wire [7:0] EstimatedDelta;

wire [7:0] EstimatedDelta2C = { ~EstimatedDelta[7], EstimatedDelta[6:2], { 2 { ~EstimatedDelta[7] } } };
wire [7:0] Accumulator2C = { ~Accumulator[7], Accumulator[6:0] };

wire [7:0] NextAccumulator2C = Accumulator2C + EstimatedDelta2C;

assign NextAccumulator = Accumulator2C[7] == EstimatedDelta2C[7] && NextAccumulator2C[7] != Accumulator2C[7]
						? { 8 { Accumulator[7] } } : { ~NextAccumulator2C[7], NextAccumulator2C[6:0] };

// -- Test control.
always_ff @(posedge iClock)
	if (StartTest)
		Accumulator <= InitialAccumulator;
	else if (UpdateAccumulator && !(&EstimatedDelta2C))
		Accumulator <= NextAccumulator;
		
		
// -- Estimation.
ProbabilityEstimator #(8) estdelta (~iTest, iClock, DeltaStream, EstimatedDelta, UpdateAccumulator);
//BitStreamAccumulator #(8) acc (~iTest, iClock, DeltaStream, EstimatedDelta, UpdateAccumulator);

// -- Sampling.
(*noprune*) logic ReadyToSample;

always_ff @(posedge iClock)
	ReadyToSample <= UpdateAccumulator;

endmodule
