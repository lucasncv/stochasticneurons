/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Top Module
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module TopStochasticTests
(
	input wire CLOCK_50,
	input wire [3:0] KEY,
	input wire [17:0] SW,
	output wire [17:0] LEDR,
	output wire [8:0] LEDG
);

// -- Control.
wire iClock = CLOCK_50;
wire iReset, iLearn;

Debouncer resetDebouncer (.iClock, .iBouncy(~KEY[0]), .oPulse(), .oState(iReset));
Debouncer learnDebouncer (.iClock, .iBouncy(~KEY[1]), .oPulse(), .oState(iLearn));

// -- Linear Feedback Shift Register.
wire [31:0] RandomStreams;
LFSR32 lfsr(iClock, RandomStreams);

// -- Address generators.
bit [2:0] AddressBitStreams;
AddressBitStreamsGenerator addr(iClock, RandomStreams[16:0], AddressBitStreams);

// -- Input stream.
wire GeneratedA, GeneratedB, GeneratedC, Generated, MarkovTest;
wire Multiplication, Sum, Sum3, Activation, Tanh;

always_comb case (SW[2:0])
	3'b000: Generated <= Multiplication;
	3'b001: Generated <= Sum;
	3'b010: Generated <= Sum3;
	3'b011: Generated <= Activation;
	3'b100: Generated <= Tanh;
	3'b101: Generated <= GeneratedA;
	3'b110: Generated <= GeneratedB;
	3'b111: Generated <= MarkovTest;
	default: Generated <= '0;
endcase

// -- 2D logic.
logic [8:0] Tick = '0;
wire [9:0] NextTick = Tick + 1;

logic DoingIt = '1;

always_ff @(posedge iClock)
	if (!DoingIt) begin
		DoingIt <= iLearn;
		Tick <= '0;
	end else if (ActualReady) begin
		DoingIt <= ~NextTick[9];
		Tick <= NextTick[8:0];
	end

// -- Inputs.
wire [4:0] InputA = { Tick[8] & (~Tick[4] | Tick[5]), Tick[3:0] };
wire [4:0] InputB = Tick[8] ? { Tick[4] | Tick[5], Tick[3:0] } : { 1'b0, Tick[7:4] };
wire [4:0] InputC = SW[17:13];

// -- Generators.
wire MOD [3], WBG [3];

BitStreamGenerator #(4) genA (iClock, InputA, RandomStreams[3:0], MOD[0]);
BitStreamGenerator #(4) genB (iClock, InputB, RandomStreams[13:10], MOD[1]);
BitStreamGenerator #(4) genC (iClock, InputC, RandomStreams[23:20], MOD[2]);

WBG #(4) wbgA (iClock, InputA, RandomStreams[3:0], WBG[0]);
WBG #(4) wbgB (iClock, InputB, RandomStreams[7:4], WBG[1]);
WBG #(4) wbgC (iClock, InputC, RandomStreams[11:8], WBG[2]);

assign GeneratedA = SW[5] ? WBG[0] : MOD[0];
assign GeneratedB = SW[5] ? WBG[1] : MOD[1];
assign GeneratedC = SW[5] ? WBG[2] : MOD[2];

// -- Multiply.
assign Multiplication = ~(GeneratedA ^ GeneratedB);

// -- Add.
StochasticAdder adder(.iA(GeneratedA), .iB(GeneratedB), .iRandomSource(RandomStreams[18]), .oStream(Sum));

// -- Markov costs test.
localparam MarkovSize = 7;

logic [MarkovSize:1] PackedMarkovInputs;

assign PackedMarkovInputs[1] = RandomStreams[24];

generate
	always @(posedge iClock)
		PackedMarkovInputs[MarkovSize:2] <= PackedMarkovInputs[MarkovSize-1:1];
endgenerate

MarkovAdder #(MarkovSize) markovTest
(
	.iClock,
	.iRandomSource(RandomStreams[23]),
	.iStreams(PackedMarkovInputs),
	.oStream(MarkovTest)
);

// -- Add 3.
MarkovAdder #(3) markov
(
	.iClock,
	.iRandomSource(RandomStreams[18]),
	.iStreams({ GeneratedA, GeneratedB, GeneratedC }),
	.oStream(Sum3)
);

// -- Tangent.
StochasticTanh tanhSum3
(
	.iClock,
	.iRandomSource(RandomStreams[27]),
	.iStream(Sum3),
	.oActivation(Activation),
	.oDerivative()
);

StochasticTanh tanhA
(
	.iClock,
	.iRandomSource(RandomStreams[27]),
	.iStream(InputA),
	.oActivation(Tanh),
	.oDerivative()
);

// -- Estimate.
// -- Estimation.
(* noprune *) reg [15:0] Prob16;
//(* noprune *) reg [7:0] Prob8;
(* noprune *) wire ActualReady;

//ProbabilityEstimator #(8) est8 (iReset, iClock, Generated, Prob8, ActualReady);
BasicEstimator #(16) est16 (.iClock, .iStream(Generated), .oProbability(Prob16), .oReady(ActualReady));

assign LEDG[0] = ActualReady;

endmodule

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module SNG #(W)
(
	input wire iClock,
	input logic [W:0] iProbability,
	input logic [W:1] iRandomSource,
	output logic oStream
);

always_ff @(posedge iClock)
	oStream <= iProbability[W] | (iProbability[W-1:0] > iRandomSource);

endmodule

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module WBG #(W)
(
	input wire iClock,
	input logic [W:0] iProbability,
	input logic [W:1] iRandomSource,
	output logic oStream
);

wire [W:1] Sequences;

assign Sequences[W] = iRandomSource[W];

generate genvar n;
	for (n = 1; n < W; n++) begin : ForEachN
		assign Sequences[n] = (&(~iRandomSource[W:n+1])) & iRandomSource[n];
	end
endgenerate

always_ff @(posedge iClock)
	oStream <= iProbability[W] | (|(Sequences & iProbability[W-1:0]));

endmodule

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module SMUX
(
	input wire iClock,
	input logic [8:0] iProbability,
	input logic [2:0] iSeed,
	output logic oStream
);

wire [7:0] iProbability8bit = iProbability[7:0];

always_ff @(posedge iClock)
	oStream <= iProbability[8] | iProbability8bit[iSeed];

endmodule

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module BasicEstimator #(W)
(
	input wire iClock,
	input logic iStream,
	output logic [W:1] oProbability,
	output logic oReady
);

// -- Tick counter.
logic [W:1] Tick = '0;

wire [W:1] NextTick;
assign { oReady, NextTick } = Tick + 1;

always @(posedge iClock)
	Tick <= NextTick;
    
// -- Accumulates bits from the bit-stream.
logic [W:1] Accumulator = '0;

always @(posedge iClock)
	Accumulator <= oReady ? '0 : Accumulator + iStream;

assign oProbability = Accumulator;

endmodule
