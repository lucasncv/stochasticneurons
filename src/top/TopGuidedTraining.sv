/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Top Module
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module TopGuidedTraining
(
	input wire CLOCK_50,
	input wire [3:0] KEY,
	input wire [17:0] SW,
	output wire [17:0] LEDR,
	output wire [8:0] LEDG
);

// -- Parameters.
localparam I = 2;
localparam O = 1;

// -- Control.
wire iClock = CLOCK_50;
wire iTrain, iReset, iLoadWeights;

Debouncer deb0 (.iClock, .iBouncy(~KEY[1]), .oPulse(iTrain), .oState());
Debouncer deb1 (.iClock, .iBouncy(~KEY[0]), .oPulse(), .oState(iReset));
Debouncer deb2 (.iClock, .iBouncy(~KEY[3]), .oPulse(iLoadWeights), .oState());

// -- Linear Feedback Shift Register.
bit [31:0] RandomStreams;
LFSR32 lfsr(iClock, RandomStreams);

// -- Learning Factor.
wire LearningFactor;
BitStreamGenerator #(4) gen(iClock, SW[17:13], RandomStreams[31:28], LearningFactor);

// -- Epoch control.
logic [9:0] CurrentEpoch = '0;
wire [9:0] TotalEpochs = SW[9:0];
wire [9:0] NextEpoch = CurrentEpoch + 1;

wire IsTraining = State == LEARN;

always_ff @(posedge iClock)
	if (iReset)
		CurrentEpoch <= '0;
	else if (FinishedEpoch)
		CurrentEpoch <= NextEpoch;
		
logic Last8th = '0;

always_ff @(posedge iClock)
	Last8th <= CurrentEpoch[3];

// -- State Machine.
typedef enum logic [1:0] { IDLE, LEARN, REST } StateCodes;
StateCodes State = IDLE;

logic [24:0] StateTick = '0;
logic [24:0] NextTick;

logic FullWait;
assign { FullWait, NextTick } = StateTick + 1;

always_ff @(posedge iClock) begin
	StateTick <= InTransition ? '0 : NextTick;
	State <= NextState;
end

// -- Chooses the next state.
StateCodes NextState;

always_comb begin
	NextState = State;
	
	case (State)
		IDLE:
			if (iTrain)
				NextState = LEARN;
		LEARN:
			if (Last8th != CurrentEpoch[3])
				NextState = REST;
		REST:
			if (CurrentEpoch >= TotalEpochs)
				NextState = IDLE;
			else if (FullWait)
				NextState = LEARN;
	endcase
	
	if (iReset)
		NextState = IDLE;
end

wire InTransition = State != NextState;

// -- Cycle control.
wire TCReady, TCNowReady, TCFirst;
wire NNBusy, NNFinishedWork, NetworkIsCooledDown;

Pulse pulse0 (.iClock, .iSignal(TCReady), .oHigh(TCNowReady), .oLow());
Pulse pulse1 (.iClock, .iSignal(NNBusy), .oHigh(), .oLow(NNFinishedWork));
//Pulse pulse2 (.iClock, .iSignal(NNFinishedWork), .oHigh(), .oLow(NetworkIsCooledDown));

// -- Training.
wire InputSet [I];
wire OutputSet [O];
wire Performance [O];
wire NNOutputs [O];

TrainingControl #(.I(I), .O(O)) training
(
	.iClock,
	.iReset(iReset || (InTransition && NextState == REST)),
	.iNext(FetchNextSet),
	.oReady(TCReady),
	.oFirstSet(TCFirst),
	.oInputSet(InputSet),
	.oOutputSet(OutputSet)
);

// -- Control stuff.
wire FinishedSet = IsTraining && NNFinishedWork;
wire FetchNextSet = FinishedSet || (InTransition && NextState == LEARN);
wire ReadyToLearn = IsTraining && TCNowReady;
wire FinishedEpoch = FinishedSet && TCFirst;

// -- Warm up between sets.
wire NetworkIsWarmedUp;

WarmUpTimer warmUp
(
	.iClock,
	.iReset(~IsTraining),
	.iStartPulse(ReadyToLearn),
	.oEndPulse(NetworkIsWarmedUp)
);

// -- Network.
NeuralNetworkControl Control
(
	.iClock,
	.iOperation(iReset ? 3'b010 : iLoadWeights ? 3'b011 : { 2'b0, NetworkIsWarmedUp }),
	.oBusy(NNBusy)
);

NeuralNetwork ann
//NeuralNetworkWithTwoHiddenLayers ann
(
	.iClock,
	.iLearningFactor(LearningFactor),
	.iInputs(InputSet),
	.iExpectedOutputs(OutputSet),
	.oActualOutputs(NNOutputs),
	.oPerformance(Performance),
	.Control(Control)
);

// -- Performance.
logic [7:0] EstimatedPerf;
logic AccReady;

ProbabilityEstimator #(8) estperf (ReadyToLearn, iClock, Performance[0], EstimatedPerf, AccReady);

// -- Output.
ProbabilityEstimator #(8) estout (ReadyToLearn, iClock, NNOutputs[0]);

// -- LEDs.
assign LEDR[9:0] = CurrentEpoch;
assign LEDG[1:0] = State;

assign LEDR[17] = StateTick[24];
assign LEDR[16] = ~StateTick[24];

endmodule

