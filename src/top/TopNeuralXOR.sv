/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Top Module
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module TopNeuralXOR
(
	CLOCK_50,
	KEY, SW, LEDG, LEDR
);

// -- The inputs.
input wire CLOCK_50;

input wire [3:0] KEY;
input wire [17:0] SW;

// -- The outputs.
output wire [17:0] LEDR;
output wire [8:0] LEDG;

// -- Control.
wire CLOCK = CLOCK_50;
wire RESET = ~KEY[0];


// -- Linear Feedback Shift Register.
bit [31:0] RandomStreams;
LFSR32 lfsr(CLOCK, RandomStreams);


// -- Address generators.
bit [2:0] AddressBitStreams;
AddressBitStreamsGenerator addr(CLOCK, RandomStreams[17:0], AddressBitStreams);

// -- Address delays.
wire [2:0] LUTADDR [1:3];
SignalDelays #(3, 3) delays(CLOCK, AddressBitStreams, LUTADDR);


// -- NN Input layer.
wire InputStreamA, InputStreamB;
BitStreamGenerator #(4) inputGen [1:0] (CLOCK, SW[7:0], RandomStreams[31:24], { InputStreamA, InputStreamB });

// -- NN Hidden layer.
wire IntermediateA;
CompleteNeuron #(3) neuron1(CLOCK, '{ 8'b11111111, 8'b11111111, 8'b11111111 }, LUTADDR, { InputStreamA, InputStreamB, 1'b1 }, IntermediateA, 1'bx);

wire IntermediateB;
CompleteNeuron #(3) neuron2(CLOCK, '{ 8'b00000000, 8'b00000000, 8'b11111111 }, LUTADDR, { InputStreamA, InputStreamB, 1'b1 }, IntermediateB, 1'bx);

// -- NN Output layer.
wire NeuralOutput;
CompleteNeuron #(3) neuron3(CLOCK, '{ 8'b11111111, 8'b11111111, 8'b00000000 }, LUTADDR, { IntermediateA, IntermediateB, 1'b1 }, NeuralOutput, 1'bx);


// -- Accumulator input.
reg AccumulatorInput;

always case (SW[17:15])
	3'b000: AccumulatorInput = InputStreamA;
	3'b001: AccumulatorInput = InputStreamB;
	3'b010: AccumulatorInput = IntermediateA;
	3'b011: AccumulatorInput = IntermediateB;
	default: AccumulatorInput = NeuralOutput;
endcase

// -- Accumulators.
wire [8:0] AccumulatorOutput;
BitStreamAccumulator #(8) acc8(RESET, CLOCK, AccumulatorInput, AccumulatorOutput, 1'bx);

// -- 8-bit Probability.
(*noprune*) logic [7:0] Probability8bit;

always_ff @(posedge CLOCK)
	Probability8bit <= AccumulatorOutput[8] ? 8'hFF : AccumulatorOutput[7:0];

// -- Round probability.
logic [3:0] RoundedProbability;

always_ff @(posedge CLOCK)
	RoundedProbability <= AccumulatorOutput[7:4] + AccumulatorOutput[3];

// -- LED feedback.
ProbabilityLEDFeedback leds(Probability8bit[7:4], LEDR);


endmodule
