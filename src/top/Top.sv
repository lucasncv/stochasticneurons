/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Top Module
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module Top
(
	CLOCK_50,
	KEY, SW, LEDG, LEDR
);

// -- The inputs.
input wire CLOCK_50;

input wire [3:0] KEY;
input wire [17:0] SW;

// -- The outputs.
output wire [17:0] LEDR;
output wire [8:0] LEDG;

// -- Control.
wire CLOCK = CLOCK_50;
wire RESET = ~KEY[0];

// -- Linear Feedback Shift Register.
bit [31:0] RandomStreams;
LFSR32 lfsr(CLOCK, RandomStreams);

// -- Address generators.
bit [2:0] AddressBitStreams;
AddressBitStreamsGenerator addr(CLOCK, RandomStreams[17:0], AddressBitStreams);

// -- LUT generator.
bit LUTStream;
BitStreamLUT genLUT(CLOCK, AddressBitStreams, SW[9:2], LUTStream);

// -- SNG.
wire SNGStream;
BitStreamGenerator #(8) gen(CLOCK, SW[9:2], RandomStreams[7:0], SNGStream);

// -- Accumulator input.
reg AccumulatorInput;

always case (SW[17:15])
	3'b000: AccumulatorInput = AddressBitStreams[0];
	3'b001: AccumulatorInput = AddressBitStreams[1];
	3'b010: AccumulatorInput = AddressBitStreams[2];
	3'b011: AccumulatorInput = LUTStream;
	3'b100: AccumulatorInput = SNGStream;
	3'b101: AccumulatorInput = RandomStreams[14];
	3'b110: AccumulatorInput = RandomStreams[16];
	3'b111: AccumulatorInput = RandomStreams[18];
endcase

// -- Accumulators.
wire AccumulatorReady4;
bit [4:0] Probability4;
BitStreamAccumulator #(4) acc4(RESET, CLOCK, AccumulatorInput, Probability4, AccumulatorReady4);

wire AccumulatorReady6;
bit [6:0] Probability6;
BitStreamAccumulator #(6) acc6(RESET, CLOCK, AccumulatorInput, Probability6, AccumulatorReady6);

wire AccumulatorReady8;
bit [8:0] Probability8;
BitStreamAccumulator #(8) acc8(RESET, CLOCK, AccumulatorInput, Probability8, AccumulatorReady8);

wire AccumulatorReady10;
bit [10:0] Probability10;
BitStreamAccumulator #(10) acc10(RESET, CLOCK, AccumulatorInput, Probability10, AccumulatorReady10);

// -- Ready.
reg AccumulatorReady;

always @(posedge CLOCK)
case (SW[1:0])
	2'b00: AccumulatorReady <= AccumulatorReady4;
	2'b01: AccumulatorReady <= AccumulatorReady6;
	2'b10: AccumulatorReady <= AccumulatorReady8;
	2'b11: AccumulatorReady <= AccumulatorReady10;
endcase

assign LEDG[0] = AccumulatorReady;

// -- LED feedback.
reg [3:0] FeedbackInput;

always @(posedge CLOCK)
case (SW[1:0])
	2'b00: FeedbackInput <= Probability4[3:0];
	2'b01: FeedbackInput <= Probability6[5:2];
	2'b10: FeedbackInput <= Probability8[7:4];
	2'b11: FeedbackInput <= Probability10[9:6];
endcase

ProbabilityLEDFeedback leds(FeedbackInput, LEDR);

endmodule
