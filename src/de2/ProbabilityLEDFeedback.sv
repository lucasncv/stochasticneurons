/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Probability LED feedback.
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module ProbabilityLEDFeedback(PROBABILITY, LED);

// -- The inputs.
input wire [3:0] PROBABILITY;

// -- The outputs.
output wire [17:0] LED;

// -- Feedback.
reg [15:0] Feedback16;
assign LED = { 1'b1, Feedback16, 1'b1 };

// -- Decoder.
always @(PROBABILITY)
case (~PROBABILITY)
	4'b0000: Feedback16 = 16'b0000000000000001;
	4'b0001: Feedback16 = 16'b0000000000000010;
	4'b0010: Feedback16 = 16'b0000000000000100;
	4'b0011: Feedback16 = 16'b0000000000001000;
	4'b0100: Feedback16 = 16'b0000000000010000;
	4'b0101: Feedback16 = 16'b0000000000100000;
	4'b0110: Feedback16 = 16'b0000000001000000;
	4'b0111: Feedback16 = 16'b0000000010000000;
	4'b1000: Feedback16 = 16'b0000000100000000;
	4'b1001: Feedback16 = 16'b0000001000000000;
	4'b1010: Feedback16 = 16'b0000010000000000;
	4'b1011: Feedback16 = 16'b0000100000000000;
	4'b1100: Feedback16 = 16'b0001000000000000;
	4'b1101: Feedback16 = 16'b0010000000000000;
	4'b1110: Feedback16 = 16'b0100000000000000;
	4'b1111: Feedback16 = 16'b1000000000000000;
endcase

endmodule
