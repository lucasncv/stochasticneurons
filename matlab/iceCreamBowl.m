function z = iceCreamBowl(x, y)
    z = -((cos((pi/2*x) .^ 2) + cos((pi/2*y) .^ 2)) .^ 2);
end