function data = scalecolumns(data)
   for i = 1:size(data, 2)
       data(:,i) = data(:,i) - min(data(:,i));
       data(:,i) = data(:,i) / max(data(:,i));
   end
   
   data = 2 * data - 1;
end
