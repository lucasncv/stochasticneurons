function o = readHex(path)
    fid = fopen(path);
    o = textscan(fid, ':%*2c%*4c%*2c%2c%*2c');
    fclose(fid);
    o = hex2dec(o{:});
end