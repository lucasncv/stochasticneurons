function plotsech()

y = zeros(1, 64);

for i = 1:64
    X = stochastic.gen((i-1)/63);
    X = stochastic.sech(X, 8);
    %X = stochastic.sum(X, stochastic.genbi(0));
    y(i) = stochastic.binbi(X) / 2 + 0.5;
end

plot((0:63) / 31.5 - 1, y);
ylim([-1 1]);
xlim([-1 1]);
grid on;
end
