# Initiate a editing sequence
begin_memory_edit -hardware_name "USB-Blaster \[USB-0\]" -device_name \
    "@1: EP3C120/EP4CE115 (0x020F70DD)"

# Write inputs
update_content_to_memory_from_file -instance_index 0 -mem_file_path \
    "inputs.mif" -mem_file_type mif

# Write outputs
update_content_to_memory_from_file -instance_index 1 -mem_file_path \
    "outputs.mif" -mem_file_type mif

# End the editing sequence
end_memory_edit
