function [X,T] = prepareGalaga(dbc)
    
    sampleCount = size(dbc, 2);
    sz = size(dbc{1,1},1);
    
    X = zeros(10 * sampleCount, sz * sz);
    T = zeros(10 * sampleCount, 10);
    
    for i = 1:10
        for j = 1:sampleCount
            X(j + (i-1) * sampleCount,:) = reshape(dbc{i,j}, [1 (sz*sz)]);
            T(j + (i-1) * sampleCount,i) = 1;
        end
    end

    X = 2 * (X / 255) - 1;
    T = 2 * T - 1;
end
