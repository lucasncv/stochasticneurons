function [perf, net] = sbptest()

    % a1, a2, b1, b2, c1, c2
    w = randw(9);
    u = zeros(1, 3);
    dw = zeros(1, 9);
    
    % tests
    X = [-1 -1; -1 1; 1 -1; 1 1];
    Y = [-1; 1; 1; -1];
    
    % 5 5 5
    % -5 -5 5
    % 5 5 -5
    %w = [1 1 -1 -1 1 1 1 1 -1];
    
    % simulation
    function y = simulate(i)
        u(1) = (dot(i, w(1:2)) + w(7)) / 3;
        u(2) = (dot(i, w(3:4)) + w(8)) / 3;
        u(3) = (dot(activate(u(1:2)), w(5:6)) + w(9)) / 3;
        y = activate(u(3));
    end

    % back-propagation
    function perf = train(i, y, n, a, z)
        o = activate(u);
        e = derive(u);
        
        do = y - o(3);
        perf = do ^ 2 / 2;
        do = do / 2;
        
        e(3) = e(3) * do;
        e(2) = e(2) * e(3) * w(6);
        e(1) = e(1) * e(3) * w(5);

        lastdw = dw;
        dw(9) = e(3);
        dw(8) = e(2);
        dw(7) = e(1);
        dw(6) = e(3) * o(2);
        dw(5) = e(3) * o(1);
        dw(4) = e(2) * i(2);
        dw(3) = e(2) * i(1);
        dw(2) = e(1) * i(2);
        dw(1) = e(1) * i(1);
        
        dw = dw * n + lastdw * a - w * z;
        w = w + dw;
        w = max(-1, min(1, w));
    end

    perf = zeros(1, 200);
    net = @(i) simulate(i);

    for i = 1:200
        indices = [1 2 3 4];%randperm(4);
        
        for j = 1:4
            pattern = X(indices(j),:);
            result = Y(indices(j));
            
            simulate(pattern);
            perf(i) = perf(i) + train(pattern, result, 1, 0, 0);
        end
        
        %if (i > 10 && perf(i) < 0.001)
        %    disp(strcat('Reached goal in ', int2str(i), ' iterations'));
        %    return;
        %end
    end
    
    disp(w);
end

function x = randw(n)
    x = 2 * rand(1, n) - 1;
end

function o = activate(u)
    o = min(1, max(-1, 1.1 * tansig(u * 5)));
end

function d = derive(u)
    d = min(1, max(0, sech(u * 4) * 0.5 - 0.05));
end
