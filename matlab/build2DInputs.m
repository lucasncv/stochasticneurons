function X = build2DInputs(n)

    X = zeros(n * n, 2);
    i = 1;
    
    for x = 1:n
        for y = 1:n
            X(i,1) = (x-1);
            X(i,2) = (y-1);
            i = i + 1;
        end
    end
    
    X = X / (n-1);
    X = 2 * X - 1;
end
