function o = signalTap(stp, columnCount)
    disp('Acquiring column 1...');
    column = alt_signaltap_run(stp, 'unsigned', 'auto_perf');

    o = zeros(size(column, 1), columnCount);
    o(:,1) = column;
    
    for i = 2:columnCount
        disp(['Acquiring column ' num2str(i) '...']);
        o(:,i) = alt_signaltap_run(stp, 'unsigned');
    end
    
    alt_signaltap_run('END_CONNECTION');
    disp('Done.');
end
