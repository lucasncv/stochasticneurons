function o = PDC(X)
    [~, o] = ADDIE(X, 256);
end

function [Y, C] = ADDIE(X, N)
    C = zeros(1, length(X));
    Y = zeros(1, length(X));
    
    y = 0;
    c = 192;
    
    for i = 1:length(X)
        inc = X(i) & ~y;
        dec = ~X(i) & y;
        add = 0;
        
        if inc
            add = 1;
        elseif dec
            add = -1;
        end
        
        Y(i) = c > (randi(N) - 1);
        y = Y(i);
        
        C(i) = min(N - 1, max(0, c + add));
        c = C(i);
    end
end
