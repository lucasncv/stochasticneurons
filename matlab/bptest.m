function [perf, net] = bptest()

    % a1, a2, b1, b2, c1, c2
    w = [1 -1 1 -1 1 -1 1 -1 1];%randw(9);
    u = zeros(1, 3);
    dw = zeros(1, 9);
    
    % tests
    X = [-1 -1; -1 1; 1 -1; 1 1];
    Y = [-1; 1; 1; -1];
    
    % 5 5 5
    % -5 -5 5
    % 5 5 -5
    %w = [5 5 -5 -5 5 5 5 5 -5];
    
    % simulation
    function y = simulate(i)        
        u(1) = propagate(i, w(1:2));
        u(1) = (u(1) + w(7)) / 3;
        u(2) = propagate(i, w(3:4));
        u(2) = (u(2) + w(8)) / 3;
        u(3) = propagate(activate(u(1:2)), w(5:6));
        u(3) = (u(3) + w(9)) / 3;
        y = activate(u(3));
    end

    % back-propagation
    function perf = train(i, y, n, a, z)
        o = activate(u);
        e = derive(u);
        
        do = (y - o(3)) / 2;
        perf = do;% ^ 2 / 2;
        
        e(3) = e(3) * do;
        e(2) = e(2) * e(3) * w(6);
        e(1) = e(1) * e(3) * w(5);

        lastdw = dw;
        dw(9) = e(3);
        dw(8) = e(2);
        dw(7) = e(1);
        dw(6) = e(3) * o(2);
        dw(5) = e(3) * o(1);
        dw(4) = e(2) * i(2);
        dw(3) = e(2) * i(1);
        dw(2) = e(1) * i(2);
        dw(1) = e(1) * i(1);
        
        dw = dw * n;% + lastdw * a - w * z;
        w = max(-1, min(1, w + dw));
    end

    hw = zeros(9, 896);
    perf = zeros(1, 896);
    net = @(i) simulate(i);

    for i = 1:896
        indices = 1:4;%randperm(4);
        
        %for j = 1:4
        j = mod(i - 1, 4) + 1;
        
            pattern = X(indices(j),:);
            result = Y(indices(j));
            
            simulate(pattern);
            hw(:,i) = w;
            perf(i) = train(pattern, result, 0.75, 0.9, 0);
        %end
        
        %if (i > 10 && perf(i) < 0.001)
        %    disp(strcat('Reached goal in ', int2str(i), ' iterations'));
        %    return;
        %end
    end
    
    plot(1:896, perf, ':', 'DisplayName', 'Performance');
    hold on;
    
    for i = 1:9
        plot(1:896, hw(i,:), '-', 'DisplayName', ['Weight ', num2str(i)]);
    end
    
    hold off;
    ylim([-1 1]);
    grid on;
    
    %disp(w);
end

function x = randw(n)
    x = 2 * rand(1, n) - 1;
end

function u = propagate(i, w)
    u = dot(i, w);
end

function o = activate(u)
    o = tansig(u * 3);
end

function d = derive(u)
    d = 0.5 * dtansig(u * 3, tansig(u * 3));
end
