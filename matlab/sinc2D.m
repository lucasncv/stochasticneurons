function z = sinc2D(x, y)
    x = 4 * pi * x;
    y = 4 * pi * y;
    z = (sin(x) .* sin(y)) ./ (x .* y);
end
