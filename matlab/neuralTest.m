
function outputs = neuralTest(inputs)
    hidden = [RealNeuron(inputs, [2 2], -1) RealNeuron(inputs, [-2 -2], 3)];
    outputs = RealNeuron(hidden, [1 1], -2);
end

function o = RealNeuron(inputs, weights, bias)
    o = step(bias + dot(inputs, weights));
end

function y = step(n)
    y = ceil(heaviside(n));
end
