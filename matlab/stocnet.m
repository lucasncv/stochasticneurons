function net = stocnet()
    net = feedforwardnet(2);
    
    P = [0 0; 0 1; 1 0; 1 1]';
    T = [0 1 1 0];
    
    net = configure(net, P, T);
    
    net.layers{1}.netInputFcn = 'stocsum';
    net.layers{1}.transferFcn = 'stansig';
    net.layers{2}.netInputFcn = 'stocsum';
    net.layers{2}.transferFcn = 'stansig';
    
    net.divideFcn = '';
    net.trainParam.goal = 1e-8;
    net.trainParam.epochs = 1000;
    
    net = train(net, P, T);
end
