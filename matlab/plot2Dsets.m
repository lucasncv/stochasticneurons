function plot2Dsets(X, T)

    n = sqrt(size(X, 1));
    r = 2 * ((0:(n-1)) / (n-1)) - 1;
    
    z = reshape(T, [n n]);

    surf(r, r, z);
    %zlim([-1 1]);
    
    xlabel('$x_1$', 'Interpreter', 'latex', 'FontSize', 14);
    ylabel('$x_2$', 'Interpreter', 'latex', 'FontSize', 14);
    zlabel('$y$', 'Interpreter', 'latex', 'FontSize', 14);

end
