function X = mult(varargin)
    X = varargin{1};
    for i=2:length(varargin)
        X = ~xor(X, varargin{i});
    end
end
