function X = sum(varargin)
    n = length(varargin);
    r = randi(n, 2048, 1);
    X = zeros(2048, 1);
    for i = 1:size(varargin{1}, 1)
        X(i) = varargin{r(i)}(i);
    end
end

function X = oosum(varargin)
    n = length(varargin);
    
    X = zeros(size(varargin{1}, 1), 1);
    c = 1;
    for i = 1:size(varargin{1}, 1)
        %c = randi(n);
        X(i) = varargin{c}(i);
        c = c + 1;
        if (c > n)
            c = 1;
        end
    end
    %return;
    
    x = 0;
    for i = 1:size(varargin{1}, 1)
        S = [X(i) x];
        x = X(i);
        X(i) = S(randi(2));
    end
end

function X = osum(varargin)

    n = length(varargin);

    %sel = randi(n, 1, length(varargin{1}));
    sel = repmat(1:n, [1 10000]);

    X = varargin{1};
    for i = 1:length(X)
        X(i) = varargin{sel(i)}(i);
    end

    return;
    
    if n == 1
        X = varargin{1};
    else
        if mod(n, 2) == 0
            varargin{n + 1} = alt();
            n = n + 1;
        end
        
        stageCount = 1 + floor((n - 3) / 2);
        lastStage = LE(varargin{1}, varargin{2}, varargin{3});

        for i = 2:stageCount
            lastStage = LE(varargin{i * 2}, varargin{i * 2 + 1}, lastStage);
        end
        
        X = lastStage;
    end
end

function X = LE(A, B, C)
    X = (C & (A | B)) | (~C & (A & B));
end

function X = alt()
    X = stochastic.genbi(0);
    %X = 0.5 * ((-1).^(1:512) + 1);
end