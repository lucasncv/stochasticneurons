function Y = tanh(X, N)
    Y = stochastic.satcounter(X, N) >= N / 2;
end
