function Y = delay(X)
    Y = circshift(X, 1, 1);
end
