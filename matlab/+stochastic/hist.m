function hist(data, inputWidth, displayWidth)
    data = round(data / 2 ^ (inputWidth - displayWidth));
    dx = 2 * double(eps(ufi(0, 1, displayWidth)));
    histogram(data * dx - 1, -1:dx:1);
    xlabel('Probability'); ylabel('Samples');
end