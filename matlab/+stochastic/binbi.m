function p = binbi(X)
    p = 2 * stochastic.bin(X) - 1;
end
