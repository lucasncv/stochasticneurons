function Y = satcounter(X, N)
    Y = 2 * X - 1;
    c = N / 2;
    
    for i = 1:size(Y, 1)
        c = min(N - 1, max(0, c + Y(i)));
        Y(i) = c;
    end
end
