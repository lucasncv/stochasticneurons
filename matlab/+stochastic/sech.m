function Y = sech(X, N)
    c = stochastic.satcounter(X, N);
    Y = c >= 2 & c < N - 2;
end