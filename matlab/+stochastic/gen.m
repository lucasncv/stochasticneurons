function X = gen(p)
    X = rand(2048, size(p, 2));
    X = round(X + repmat(p - 0.5, 2048, 1));
end
