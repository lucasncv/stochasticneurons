function X = genbi(p)
    X = rand(2048, size(p, 2));
    X = round(X + repmat(p / 2, 2048, 1));
end
