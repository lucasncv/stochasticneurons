function p = bin(X)
    p = sum(X) / size(X, 1);
end