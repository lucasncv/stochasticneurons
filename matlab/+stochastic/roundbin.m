function o = roundbin(data, inputWidth, displayWidth)
    data = round(data / 2 ^ (inputWidth - displayWidth));
    dx = 2 * double(eps(ufi(0, 1, displayWidth)));
    o = data * dx - 1;
end
