function X = dot(varargin)
    n = length(varargin);
    
    
    
    
    r = randi(n, 2048, 1);
    X = zeros(2048, 1);
    for i = 1:2048
        X(i) = varargin{r(i)}(i);
    end
end