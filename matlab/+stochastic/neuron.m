function X = neuron(inputs, weights, bias) % weights and bias are binary, inputs are streams
    
    n = length(weights);
    streams = cell(1, n + 1);
    
    for i = 1:n
        streams{i} = stochastic.mult(stochastic.genbi(weights{i}), inputs{i});
    end
    
    streams{n + 1} = stochastic.genbi(bias);
    
    X = stochastic.sum(streams{:});
    X = stochastic.tanh(X, 16);
end
