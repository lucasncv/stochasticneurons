function salvaMIF(nome, x, t)

[n, m] = size(x);
x = 255 * (x + 1) / 2;

s=sprintf('%s_x.mif',nome);
ain=fopen(s,'w');
fprintf(ain,'DEPTH = %d;\n', m * n + 1);
fprintf(ain,'WIDTH = 8;\nADDRESS_RADIX = HEX;\nDATA_RADIX = HEX;\n');
fprintf(ain,'CONTENT\nBEGIN\n');

fprintf(ain,'00000000 : %s;\n', dec2hex(n, 2));

for i=1:n
    fprintf(ain,'%s :',dec2hex((i - 1) * m + 1, 8));
    for j=1:m
        fprintf(ain,' %s',dec2hex(uint8(x(i, j)), 2));
    end
    fprintf(ain,';\n');
end

fprintf(ain,'END;\n');
fclose(ain);

[n, m] = size(t);

s=sprintf('%s_t.mif',nome);
ain=fopen(s,'w');
fprintf(ain,'DEPTH = %d;\n', m * n);
fprintf(ain,'WIDTH = 1;\nADDRESS_RADIX = HEX;\nDATA_RADIX = HEX;\n');
fprintf(ain,'CONTENT\nBEGIN\n');

for i=1:n
    fprintf(ain,'%s :',dec2hex((i - 1) * m, 8));
    for j=1:m
        fprintf(ain,' %s',dec2hex(uint8(t(i, j) > 0), 1));
    end
    fprintf(ain,';\n');
end

fprintf(ain,'END;\n');
fclose(ain);

end
