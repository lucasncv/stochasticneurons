function y = signalTestPlot(n)
    stp = 'D:/Dev/FPGA/StochasticNeurons/Top.stp';
    Z = zeros(512, 2);
    
    for i = 1:n
        disp('Waiting...');
        X = alt_signaltap_run(stp, 'unsigned', 'signal_test');
        Z = Z + double(X) / n;
    end
    
    alt_signaltap_run('END_CONNECTION');
    
    x = X(65:320,1);
    y = Z(65:320,2);
    
    x = double(x) / 128 - 1;
    y = double(y) / 128 - 1;
    
    plot(x, y);
    ylim([-1 1]);
    xlim([-1 1]);
    
    xlabel('$x$', 'Interpreter', 'latex', 'FontSize', 12);
    ylabel('$x^2$', 'Interpreter', 'latex', 'FontSize', 12);
    grid on;
end
