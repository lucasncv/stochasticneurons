function plotClassificationBatch(ratios, hiddenSizes)
    ratios = ratios * 100;
    
    avg = mean(ratios, 2);
    %v = std(ratios, [], 2);
    
    upper = max(ratios, [], 2) - avg;
    lower = avg - min(ratios, [], 2);
    
    errorbar(hiddenSizes, avg, lower, upper, 'LineWidth', 1);
    ylim([0 100]);
    
    xlabel('Neur\^onios em cada Camada Oculta', 'Interpreter', 'latex', 'FontSize', 14);
    ylabel('Taxa de Acerto M\''edia', 'Interpreter', 'latex', 'FontSize', 14);
end