function n = testRFSM()

X = stochastic.genbi(0);

N = 4;
c = 1;
n = zeros(1, N);

for i = 1:size(X, 1)
    if (X(i))
        if (c == N)
            c = 1;
        else
            c = c + 1;
        end
    elseif (c == 1)
        c = N;
    else
        c = c - 1;
    end
    
    n(c) = n(c) + 1;
end

n = n / size(X, 1);
n = n - 1/N;
n = n .^ 2;
plot(n);
ylim([0 0.1]);

end
