function o = testADDIE()

    N = 256;
    
    A = stochastic.genbi(0.5);
    B = stochastic.genbi(0);
    
    c = stochastic.gen(0);
    
    INC = A & B;
    DEC = (~A) & (~B);
    
    c(1) = 0;
    
    for i = 2:length(c)
        c(i) = c(i-1) + INC(i) - DEC(i);
        c(i) = min(N - 1, max(0, c(i)));
    end
    
    E = c(i) > (randi(N, length(c), 1) - 1);
    
    plot(c);
    o = stochastic.binbi(E);
end
