function a = apply(n,param)

% Copyright 2012 The MathWorks, Inc.

a = 2 ./ (1 + exp(-8*n)) - 1;

