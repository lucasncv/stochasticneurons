function saveWeights(net, name)
    layerCount = length(net);
    
    weights = [];
    
    for i = 2:layerCount
        w = net(i).weights(:,1:net(i).synapseCount-1);
        b = net(i).weights(:,net(i).synapseCount);
        weights = [weights reshape(w', [1 numel(w)]) b'];
    end
    
    x = uint8(255 * (weights + 1) / 2);
    wn = length(x);
    
    s=sprintf('%s_w.mif', name);
    ain=fopen(s,'w');
    fprintf(ain,'DEPTH = %d;\n', wn);
    fprintf(ain,'WIDTH = 8;\nADDRESS_RADIX = HEX;\nDATA_RADIX = HEX;\n');
    fprintf(ain,'CONTENT\nBEGIN\n');
    
    for i=1:wn
        fprintf(ain,'%s : %s;\n',dec2hex(i - 1, 4), dec2hex(x(i), 2));
    end

    fprintf(ain,'END;\n');
    fclose(ain);
end
