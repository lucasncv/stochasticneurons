function plotnet(sim)
    r = -1:0.01:1;
    z = zeros(length(r), 1);
    
    for x = 1:length(r)
        o = sim(r(x));
        z(x) = o;
    end
    
    plot(r, z);
    ylim([-1 1]);
    
    %xlabel('$x_1$', 'Interpreter', 'latex', 'FontSize', 14);
    %ylabel('$x_2$', 'Interpreter', 'latex', 'FontSize', 14);
    %zlabel('$y$', 'Interpreter', 'latex', 'FontSize', 14);
end
