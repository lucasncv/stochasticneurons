classdef SNG < handle
    properties
        Probability % bits of the probability number
        LFSR % the linear feedback shift register
        StageBits
    end
    
    methods
        function self = SNG(Probability, LFSR)
            self.Probability = bitget(uint8(Probability), 1:1:4);
            self.LFSR = LFSR;
            self.StageBits = zeros(1, 4);
        end
        
        function bit = Tick(self)
            
            randomBits = arrayfun(@(~) self.LFSR.Tick(), self.StageBits);
            bits = Modulator(self.StageBits, self.Probability, randomBits);
            
            self.StageBits(2:4) = bits(1:3);
            bit = bits(4);
        end
    end
end

function output = Modulator(input, modulation, carrier)
    a = input & modulation;
    b = modulation & carrier;
    c = input & carrier;
    output = a | b | c;
end
