function d = calcdv(p, a, b)

pn = stomul(p, 160);
pn = pn / 128 - 1;

dw = stosub(b, a);
dw = dw / 128 - 1;

d = dw/pn;

end
