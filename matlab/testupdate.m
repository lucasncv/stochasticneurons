function testupdate()

P = stochastic.genbi(1);
N = stochastic.genbi(-1);

p = zeros(1, 1024);
n = zeros(1, 1024);

for i = 1:1024
    D = stochastic.genbi(0.01);
    S = stochastic.genbi(0);
    
    P = (P & S) | (D & (~S));
    p(i) = stochastic.binbi(P) * 2;
    P = stochastic.genbi(p(i));
    
    N = (N & S) | (D & (~S));
    n(i) = stochastic.binbi(N) * 2;
    N = stochastic.genbi(n(i));
end

plot(1:1024, p, 1:1024, n);
ylim([-1 1]);

end
