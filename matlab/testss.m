function o = testss()
    %o = zeros(2, 8 * 65);
    o = zeros(2, 17);
    
    for j = 0:16
        p = 2 * (j / 16) - 1;
         
        for i = 1:32
            A = bistc(p);
            X = sech(A, 8);

            o(2, j + 1) = o(2, j + 1) + bibin(X);
        end
        
        o(1, j + 1) = p;
        o(2, j + 1) = o(2, j + 1) / 32;
    end
end

function Y = delay(X)
    Y = circshift(X, 1, 2);
end

function X = stc(p)
    X = round(rand(1, 512) + (p - 0.5));
end

function p = bin(X)
    p = sum(X) / size(X, 2);
end

function Y = cbin(X, N)
    X = counter(X, N);
    Y = X / N;
end

function X = bistc(p)
    X = stc((p + 1) / 2);
end

function p = bibin(X)
    p = 2 * bin(X) - 1;
end

function Y = div(A, B, N)
    Y = A;
    c = 0;
    q = 0;
    
    for i = 1:size(Y, 2)
        Inc = A(i);
        Dec = B(i) & q;
        c = min(N - 1, max(0, c + Inc - Dec));
        Y(i) = q;
    end
end

function Y = ssum2(A, B)
    Y = 0.5 * ((-1).^(1:512) + 1);
    Y = (Y & A) | (~Y & B);
end

function Y = ssum2b(A, B)
    Y = 0.5 * ((-1).^(0:511) + 1);
    Y = (Y & A) | (~Y & B);
end

function Y = ssum4(A, B, C, D)
    A = ssum2(A, B);
    A = ssum2(C, A);
    Y = ssum2(A, D);
end

function Y = addsat(A, B, C)
    Y = (A + B + C) > 1;
end

function Y = altstream()
    Y = 0.5 * ((-1).^(1:512) + 1);
end

function Y = act(A, B, C, D)
    Z = 0.5 * ((-1).^(1:512) + 1);
    Y = (A + B + C) > 1;
end

function X = ustc(E, e)
    X = stc(E / (e + E));
end

function E = ubin(X, e)
    p = bin(X); E = e * p / (1 - p);
end

function Q = JK(J, K)
    Q = J;
    q = 0;
    
    for i = 1:size(Q, 2)
        q = (J(i) & ~q) | (~K(i) & q);
        Q(i) = q;
    end
end

function Y = stanh(X, N)
    Y = counter(X, N) >= N / 2;
end

function Y = sech(X, N)
    C = counter(X, N);
    Y = C >= 2 & C < (N - 2);
    Y = ssum2(Y, stc(1));
end

function Y = sdouble(X, N)
    Y = counter(X, N) >= N / 2;
end

function Y = counter(X, N)
    Y = 2 * X - 1;
    c = 0;
    
    for i = 1:size(Y, 2)
        c = min(N - 1, max(0, c + Y(i)));
        Y(i) = c;
    end
end
