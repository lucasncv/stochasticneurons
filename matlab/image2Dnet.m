function image2Dnet(sim)

    r = -1:0.1:1;
    z = zeros(length(r));
    
    for x = 1:length(r)
        for y = 1:length(r)
            o = sim([r(x) r(y)]);
            z(x, y) = o;
        end
    end
    
    pcolor(r, r, z);

    xlabel('$x_1$', 'Interpreter', 'latex', 'FontSize', 14);
    ylabel('$x_2$', 'Interpreter', 'latex', 'FontSize', 14);
    axis square;
    %shading interp;
end
