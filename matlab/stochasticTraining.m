function [net, sim, perf, ratio] = stochasticTraining(layerSizes, x, t, varargin)

    % -- Check arguments
    p = inputParser;
    addParameter(p, 'LearningFactor', 0.75, @isnumeric);
    addParameter(p, 'EpochCount', 500, @isnumeric);
    addParameter(p, 'CalculateRatio', 'off');
    addParameter(p, 'PlotPerformance', 'on');
    
    parse(p, varargin{:});
    parameters = p.Results;
    
    % -- Setup the network
    setSize = size(x, 1);
    layerCount = length(layerSizes);
    layers = struct('size', num2cell(layerSizes));
    
    for i = 2:layerCount
        layers(i).synapseCount = layers(i-1).size + 1;
        layers(i).potential = zeros(layers(i).size, 1);
        layers(i).activation = zeros(layers(i).size, 1);
        layers(i).localError = zeros(layers(i).size, 1);
        layers(i).weights = 2 * rand(layers(i).size, layers(i).synapseCount) - 1;
    end
    
    % -- Feedforward
    function outputs = simulate(inputs)
        
        layers(1).activation = inputs';

        for l = 2:layerCount
            
            % Calculate action potential
            layers(l).potential = layers(l).weights * [layers(l - 1).activation; 1];
            layers(l).potential = layers(l).potential / layers(l).synapseCount;
            
            % Calculate activation
            layers(l).activation = activate(layers(l).potential);
            
        end
       
        outputs = layers(layerCount).activation';
        
    end

    % -- Backpropagation
    function perf = adapt(inputs, outputs)
        
        o = simulate(inputs);
        errors = (outputs - o) / 2;
        perf = sum(errors .^ 2);
        errors = errors';

        for l = layerCount:-1:2
            
            % Calculate local errors
            layers(l).localError = derive(layers(l).potential) .* errors;
            
            % Calculate delta weights
            dw = layers(l).localError * [layers(l - 1).activation' 1];

            %dw = double(dw > 0.01) - double(dw < -0.01);
            dw = dw * parameters.LearningFactor;
                
            % Update weights
            layers(l).weights = max(-1, min(1, layers(l).weights + dw));

            % Back-propagate
            errors = layers(l).weights(:,1:layers(l-1).size)' * layers(l).localError;
            errors = errors / layers(l).size;
            
        end
    end

    % -- Training
    perf = zeros(1, parameters.EpochCount);
    
    for e = 1:parameters.EpochCount
        
        indices = 1:setSize;%randperm(setSize);
        
        for i = indices%1:setSize
            perf(e) = perf(e) + adapt(x(i, :), t(i, :));
        end
        
        perf(e) = perf(e) / setSize;
    end
    
    % -- Check ratio of correct classifications
    if strcmp(parameters.CalculateRatio, 'on')
        ratio = 0;

        for i = 1:setSize
            y = simulate(x(i, :));
            [~, a] = max(y);
            [~, b] = max(t(i, :));
            ratio = ratio + (a == b);
        end

        ratio = ratio / setSize;
    end
    
    % -- Plot
    if strcmp(parameters.PlotPerformance, 'on')
        plot(perf);
        ylim([0 1]);
    end

    % -- Finish
    net = layers;
    sim = @(i) simulate(i);
    perf = perf(parameters.EpochCount);

end

function o = activate(u)
    o = tansig(u * 6);
end

function d = derive(u)
    d = 0.5 * dtansig(u * 6, tansig(u * 6));
end
