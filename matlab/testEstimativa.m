function testEstimativa()
    stp = 'D:/Dev/FPGA/StochasticNeurons/Top.stp';
    %res = [4 8 12 16];
    
    X = alt_signaltap_run(stp, 'unsigned', 'auto_estimate');
    X = X(65:353,:);
    
    %X = X(:,1);
    
    Z = zeros(17, 17);
    
    for i = 1:289
        x = min(17, X(i, 2) + 1);
        y = min(17, X(i, 3) + 1);
        Z(x,y) = X(i, 1);
    end
    
    %Z = reshape(X(:,1), [16,16]);
    
    Z = double(Z) / (2 ^ 16);
    Z = 2 * Z - 1;
    
    %X = double(X) / (2 ^ 16);

    %plot(X);
    alt_signaltap_run('END_CONNECTION');
    
    %histogram(X, 0.32:0.0005:0.34);
    
    xy = -1:0.125:1;
    surf(xy, xy, Z);
    
    %ylim([0 100]);
    %xlim([0 500]);
    zlim([-1 1]);
    
    %set(gca,'XGrid','on');
    %set(gca,'YGrid','on');
    
    %xlabel('Probabilidade Estimada', 'Interpreter', 'latex', 'FontSize', 12);
    %ylabel('Amostragens', 'Interpreter', 'latex', 'FontSize', 12);
    
    xlabel('$x_1$', 'Interpreter', 'latex', 'FontSize', 12);
    ylabel('$x_2$', 'Interpreter', 'latex', 'FontSize', 12);
    zlabel('$x_1 \times x_2$', 'Interpreter', 'latex', 'FontSize', 12);
end
