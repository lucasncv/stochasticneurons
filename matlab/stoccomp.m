function stoccomp(p)

T = zeros(1, 100);

for i = 1:100
    X = double(rand(65536, 1) <= p);
    C = double(stochastic.satcounter(X, 8) > 3);

    y = sum(C(65280:65536));
    y = floor(y / 32);
    
    T(i) = (y >= 6) - (y <= 2);
end

plot(T);
ylim([-2 2]);

end