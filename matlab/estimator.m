function C = estimator(T)

C = zeros(T, 1);
c = sum(gen(2 ^ 11));

for t = 2:T
    c = c + sum(gen(2 ^ 11));
    C(t) = round(c / (2 ^ 3)) / (2 ^ 7);
    c = floor(c / 2);
end

end

function o = gen(n)
    o = rand(n, 1) >= 0.5;
end
