function plotTesting()
    stp = 'D:/Dev/FPGA/StochasticNeurons/Top.stp';
    
    X = alt_signaltap_run(stp, 'unsigned', 'auto_testing');
    alt_signaltap_run('END_CONNECTION');
    
    X = X(513:4096,:);
    X = double(X) / 128 - 1;
    
    scatter3(X(:,1), X(:,2), X(:,3));
end
