function [X, T] = spiralQ()

    a = 2 * (0:19) / 19 - 1;
    X = zeros(length(a), 1);
    T = zeros(length(a), 1);
    
    for i = 1:length(a)
        X(i) = a(i);
        T(i) = -cos(5 * pi * (a(i) + 1) / 2);
    end
    
end