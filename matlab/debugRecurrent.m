function debugRecurrent()
    stp = 'D:/Dev/FPGA/StochasticNeurons/Top.stp';
    
    X = alt_signaltap_run(stp, 'unsigned', 'auto_recurrent');
    alt_signaltap_run('END_CONNECTION');
    
    X = sum(X) / 2048;
    X = 2 * X - 1;
    
    fprintf('Input: %.2f\n', X(1));
    fprintf('Context: %.2f %.2f %.2f\n', X(2), X(3), X(4));
    fprintf('Hidden Potentials: %.2f %.2f %.2f\n', X(5), X(6), X(7));
    fprintf('Hidden Activations: %.2f %.2f %.2f\n', X(8), X(9), X(10));
    fprintf('Output Potential: %.2f\n', X(11));
end
