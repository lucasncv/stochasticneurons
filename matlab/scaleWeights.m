function net = scaleWeights(net, s)

net.IW{1} = net.IW{1} * s;
net.LW{2, 1} = net.LW{2, 1} * s;
net.b{1} = net.b{1} * s;
net.b{2} = net.b{2} * s;

end
