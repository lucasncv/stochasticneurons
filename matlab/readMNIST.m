function images = readMNIST(path, n)

ain=fopen(path,'rb');

fread(ain, 4, 'int', 'ieee-be');

images = zeros(28,28,n);

for i = 1:n
    pixels = fread(ain, 784, 'uint8', 'ieee-be');
    image = reshape(pixels, [28 28])';
    images(:,:,i) = image;
end

fclose(ain);

end
