function [a, o] = testadd()
    X = stochastic.genbi(0);
    a = floor(sum(X) / 8);
    
    %for i = 1:128
        Y = stochastic.delay(X);
        X = adds(X, Y);
    %end
    
    o = floor(sum(X) / 8);
end