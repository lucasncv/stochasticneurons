function testTan()
    stp = 'D:/Dev/FPGA/StochasticNeurons/Top.stp';
    %res = [4 8 12 16];
    
    X = alt_signaltap_run(stp, 'unsigned', 'auto_estimate');
    X = X(65:321,1);
    X = double(X) / (2 ^ 8);
    X = 2 * X - 1;

    xy = (0:256) / 128 - 1;
    plot(xy, X);
    alt_signaltap_run('END_CONNECTION');
    
    %histogram(X, 0.32:0.0005:0.34);
    
    %surf(xy, xy, Z);
    
    ylim([-1 1]);
    xlim([-1 1]);
    %zlim([-1 1]);
    
    %set(gca,'XGrid','on');
    %set(gca,'YGrid','on');
    
    %xlabel('Probabilidade Estimada', 'Interpreter', 'latex', 'FontSize', 12);
    %ylabel('Amostragens', 'Interpreter', 'latex', 'FontSize', 12);
end
