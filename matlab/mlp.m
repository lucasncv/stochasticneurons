function net = mlp()
    net = feedforwardnet(2);
    P = [0 0; 0 1; 1 0; 1 1; 0 0; 0 1; 1 0; 1 1; 0 0; 0 1; 1 0; 1 1; 0 0; 0 1; 1 0; 1 1]';
    T = [0 1 1 0 0 1 1 0 0 1 1 0 0 1 1 0];
    
    net = configure(net, P, T);
    net.trainParam.goal = 1e-8;
    net.trainParam.epochs = 10000;
    net = train(net, P, T);
end
