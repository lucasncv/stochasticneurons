function c = netcost(layers)

    c = (34 * layers(1) + 5) * layers(2);
    for i = 3:length(layers)
        c = c + (36 * layers(i-1) + 34 + 5) * layers(i);
    end

end
