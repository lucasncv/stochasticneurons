function [net, sim] = stochasticTrainingOld(layerSizes, x, t)

    % -- Parameters
    learningFactor = 0.25;
    epochCount = 100;
    
    % -- Setup the network
    setSize = size(x, 1);
    layerCount = length(layerSizes);
    layers = struct('size', num2cell(layerSizes));
    
    for i = 2:layerCount
        layers(i).synapseCount = layers(i-1).size + 1;
        layers(i).potential = zeros(1, layers(i).size);
        layers(i).activation = zeros(1, layers(i).size);
        layers(i).localError = zeros(1, layers(i).size);
        
        for j = 1:layers(i).size
            layers(i).neurons(j).weights = randw(layers(i).synapseCount);
        end
    end
    
    % -- Feedforward
    function outputs = simulate(inputs)
        
        layers(1).activation = inputs;

        for l = 2:layerCount
            for n = 1:layers(l).size
                 layers(l).potential(n) = potential(layers(l - 1).activation, layers(l).neurons(n).weights);
            end

            layers(l).activation = activate(layers(l).potential);
        end
       
        outputs = layers(layerCount).activation;
    end

    % -- Backpropagation
    function perf = adapt(inputs, outputs)
        
        o = simulate(inputs);
        errors = (outputs - o) / 2;
        perf = sum(errors .^ 2);
        
        for l = layerCount:-1:2
            
            layers(l).localError = derive(layers(l).potential) .* errors;
            errors = zeros(1, layers(l - 1).size);
            
            for n = 1:layers(l).size
                % Deltas
                dw = layers(l - 1).activation * layers(l).localError(n);
                db = layers(l).localError(n);
                
                % Update
                onOff = 1; % double(rand() > 0.5);
                layers(l).neurons(n).weights = layers(l).neurons(n).weights + [dw db] * learningFactor * onOff;
                
                % Back-propagate
                errors = errors + layers(l).neurons(n).weights(1:layers(l - 1).size) * layers(l).localError(n);
            end
            
            errors = errors / layers(l).size;
            
        end
    end

    % -- Training
    perf = zeros(1, epochCount);
    
    for e = 1:epochCount
        for i = 1:setSize
            perf(e) = perf(e) + adapt(x(i, :), t(i, :));
        end
        
        perf(e) = perf(e) / setSize;
    end
    
    % -- Plot
    plot(perf);
    ylim([0 1]);

    % -- Finish
    net = layers;
    sim = @(i) simulate(i);

end

function u = potential(x, w)
    u = dot([x 1], w) / length(w);
end

function x = randw(n)
    x = 2 * rand(1, n) - 1;
end

function o = activate(u)
    o = tansig(u * 6);
end

function d = derive(u)
    d = 0.5 * dtansig(u * 6, tansig(u * 6));
end
