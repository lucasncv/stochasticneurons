function o = netTest()

    net = feedforwardnet(2);
    
    P = [0 0; 0 1; 1 0; 1 1]';
    T = [0 1 1 0];
    
    net = configure(net, P, T);
    net.divideFcn = '';
    
    net.layers{1}.netInputFcn = 'stosum';
    
    net = train(net, P, T);
    o = net;
    
end
