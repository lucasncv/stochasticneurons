function Z = guidedTraining(X, T, sampleCount)
    %loadTrainingSet(X, T);
    
    sz = size(X, 1);
    n = sqrt(sz);
    r = 2 * ((0:(n-1)) / (n-1)) - 1;
    
    z = zeros(n, n);
    h = surf(r, r, z);
    h.ZDataSource = 'z';
    drawnow();

    stp = 'D:/Dev/FPGA/StochasticNeurons/Top.stp';
    
    for i = 1:sampleCount
        disp('Waiting...');
        Z = alt_signaltap_run(stp, 'unsigned', 'auto_guided');
        
        Y = Z(65:65 + sz - 1,4);
        P = Z(65:65 + sz - 1,5);
        
        Y = 2 * (double(Y) / 255) - 1;
        P = 2 * (double(P) / 255) - 1;
        
        z = reshape(Y, [n n]);
        
        refreshdata(h, 'caller');
        zlim([-1 1]);
        drawnow();
    end

    alt_signaltap_run('END_CONNECTION');
end
