function [Y, C] = ADDIE(X, N)
    C = zeros(1, length(X));
    Y = zeros(1, length(X));
    
    c = 0;
    y = 0;
    
    UD = 2 * X - 1;
    
    for i = 1:length(X)
        
        
        Y(i) = c > (randi(N) - 1);
        y = Y(i);
        
        CE = xor(y, X(i));
        
        if CE
            c = min(N - 1, max(0, c + UD(i)));
        end
        
        C(i) = c;
    end
end
