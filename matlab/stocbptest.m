function [perf, net] = stocbptest()

    % a1, a2, b1, b2, c1, c2
    w = ones(1, 9) * -1;%randw(9);
    u = zeros(2048, 3);
    
    % tests
    X = [-1 -1; -1 1; 1 -1; 1 1];
    Y = [-1; 1; 1; -1];
    
    % 5 5 5
    % -5 -5 5
    % 5 5 -5
    %w = [5 5 -5 -5 5 5 5 5 -5];
    
    % simulation
    function y = simulate(i)
        si = stochastic.genbi(i);
        sw = stochastic.genbi(w);
        
        sw(:,1) = stochastic.mult(si(:,1), sw(:,1));
        sw(:,2) = stochastic.mult(si(:,2), sw(:,2));
        sw(:,3) = stochastic.mult(si(:,1), sw(:,3));
        sw(:,4) = stochastic.mult(si(:,2), sw(:,4));
        
        u(:,1) = stochastic.sum(sw(:,1), sw(:,2), sw(:,7));
        u(:,1) = stochastic.sum(sw(:,3), sw(:,4), sw(:,8));
        
        sw(:,5) = stochastic.mult(stochastic.tanh(u(:,1), 8), sw(:,5));
        sw(:,6) = stochastic.mult(stochastic.tanh(u(:,2), 8), sw(:,6));
        
        u(:,3) = stochastic.sum(sw(:,5), sw(:,6), sw(:,9));
        y = stochastic.binbi(stochastic.tanh(u(:,3), 8));
    end

    % back-propagation
    function perf = train(i, y, n)
        o = u;
        o(:,1) = stochastic.tanh(u(:,1), 8);
        o(:,2) = stochastic.tanh(u(:,2), 8);
        o(:,3) = stochastic.tanh(u(:,3), 8);
        
        e = u;
        e(:,1) = stochastic.sech(u(:,1), 8);
        e(:,2) = stochastic.sech(u(:,2), 8);
        e(:,3) = stochastic.sech(u(:,3), 8);
        
        sy = stochastic.genbi(y);
        sw = stochastic.genbi(w);
        si = stochastic.genbi(i);
        
        do = stochastic.sum(sy, ~o(:,3));
        perf = stochastic.binbi(do);

        e(:,3) = stochastic.mult(e(:,3), do);
        e(:,2) = stochastic.mult(e(:,2), e(:,3), sw(:,6));
        e(:,1) = stochastic.mult(e(:,1), e(:,3), sw(:,5));

        sn = stochastic.genbi(n);
        
        w(9) = update(stochastic.sum(stochastic.mult(e(:, 3), sn), sw(:,9)));
        w(8) = update(stochastic.sum(stochastic.mult(e(:, 2), sn), sw(:,8)));
        w(7) = update(stochastic.sum(stochastic.mult(e(:, 1), sn), sw(:,7)));
        
        w(6) = update(stochastic.sum(stochastic.mult(o(:,2), e(:, 3), sn), sw(:,6)));
        w(5) = update(stochastic.sum(stochastic.mult(o(:,1), e(:, 3), sn), sw(:,5)));
        w(4) = update(stochastic.sum(stochastic.mult(si(:,2), e(:, 2), sn), sw(:,4)));
        w(3) = update(stochastic.sum(stochastic.mult(si(:,1), e(:, 2), sn), sw(:,3)));
        w(2) = update(stochastic.sum(stochastic.mult(si(:,2), e(:, 1), sn), sw(:,2)));
        w(1) = update(stochastic.sum(stochastic.mult(si(:,1), e(:, 1), sn), sw(:,1)));
    end

    hw = zeros(9, 896);
    perf = zeros(1, 896);
    net = @(i) simulate(i);

    for i = 1:896
        indices = 1:4;%randperm(4);
        
        %for j = 1:4
        j = mod(i - 1, 4) + 1;
        
            pattern = X(indices(j),:);
            result = Y(indices(j));
            
            simulate(pattern);
            hw(:,i) = w;
            perf(i) = train(pattern, result, 0.5);
        %end
        
        %if (i > 10 && perf(i) < 0.001)
        %    disp(strcat('Reached goal in ', int2str(i), ' iterations'));
        %    return;
        %end
    end
    
    plot(1:896, perf);
    hold on;
    
    for i = 1:9
        plot(1:896, hw(i,:));
    end
    
    hold off;
    ylim([-1 1]);
    grid on;
    
    %disp(w);
end

function w = update(hw)
    w = max(-1, min(1, stochastic.binbi(hw) * 2));
end

function x = randw(n)
    x = 2 * rand(1, n) - 1;
end

function u = propagate(i, w)
    u = dot(i, w);
end

function o = activate(u)
    o = tansig(u * 3);
end

function d = derive(u)
    d = 0.5 * dtansig(u * 3, tansig(u * 3));
end
