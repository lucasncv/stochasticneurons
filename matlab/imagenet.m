function imagenet(sim)

    r = -1:0.1:1;
    z = zeros(length(r));
    
    for x = 1:length(r)
        for y = 1:length(r)
            o = sim([r(x) -r(y)]);
            [~,n] = max(o);
            z(x, y) = n * 128;
        end
    end
    
    image(z,'CDataMapping','scaled');
    
end
