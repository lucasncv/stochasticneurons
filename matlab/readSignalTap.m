%
% The first row will is the clock cycle.
%
function data = readSignalTap(filename)
    fp = fopen(filename, 'r');
    
    for i = 1:4
        fgetl(fp);
    end
    
    signalIndices = fscanf(fp, '%d %*[^\n]', [1 Inf]);
    signalCount = size(signalIndices, 2);
    
    line = '';
    while ~strcmp(line, 'sample')
        line = fgetl(fp);
    end
    fgetl(fp);
    
    data = fscanf(fp, '%d', [(signalCount + 1) Inf]);
end
