function data = arrayFromFile(file, fractionLength)
    fp = fopen(file, 'r');
    data = fscanf(fp, '%d', [1 Inf]);
    fclose(fp);
    
    dx = double(eps(ufi(0, 1, fractionLength)));
    
    n = (2 ^ fractionLength - 1);
    x = (0:16)/16;
    data = data / n;
    
    %data = vec2mat(data, 32).';
    %data = mean(data);
    
    
    data = 2 * data - 1;
    x = 2 * x - 1;
    
    plot(x, data); xlim([-1 1]); ylim([-1 1]);
    
    data = [x; data];
end
