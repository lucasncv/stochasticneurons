function v = calcVar(u, d)

v = u - d;
return;

a = u * (1 - d);
b = d * (1 - u);

v = a + b - (a - b) ^ 2;

end