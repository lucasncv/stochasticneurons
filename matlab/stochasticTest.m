function o = stochasticTest()

    o = zeros(1, 256);

    rnd = LFSR;
    
    function [a, b, ab] = testMultiplication()
        inputA = SNG(9, rnd);
        inputB = SNG(6, rnd);
        
        sample = zeros(3, 32);

        for j = 1:64
            sample(1, j) = inputA.Tick();
            sample(2, j) = inputB.Tick();
            sample(3, j) = sample(1, j) & sample(2, j);
        end
        
        a = stochastic2Binary(sample(1,:));
        b = stochastic2Binary(sample(2,:));
        ab = stochastic2Binary(sample(3,:));
    end

    [~, ~, o] = arrayfun(@(~, ~, ~) testMultiplication(), o, o, o);
    histogram(o);
    xlim([0 1]);
end

function o = ActivationFunc(inputs)
    o = double(sum(inputs) > size(inputs, 2) / 2);
end

function o = real2Stochastic(probability, streamSize)
    o = floor(rand(1, streamSize) + probability);
end

function o = stochastic2Real(stream)
    o = sum(stream) / size(stream, 2);
end
