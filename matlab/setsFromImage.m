function [X, T] = setsFromImage(img)

    n = size(img, 1);

    T = zeros(n * n, 1);
    X = zeros(n * n, 2);
    i = 1;
    
    for x = 1:n
        for y = 1:n
            X(i,1) = (x-1);
            X(i,2) = (y-1);

            T(i) = img(x, y, 1);
            i = i + 1;
        end
    end
    
    X = X / (n-1);
    X = 2 * X - 1;
    T = 2 * T / 255 - 1;
end
