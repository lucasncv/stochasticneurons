function scatterNet(sim)

    r = -1:0.1:1;
    z = zeros(length(r));
    
    for x = 1:length(r)
        for y = 1:length(r)
            o = sim([r(x) r(y)]);
            [~,n] = max(o);
            z(x, y) = n;
        end
    end
    
    surf(r, r, z);
    %zlim([-1 1]);
end
