function samples = sampleSignal(signal, n)
    samples = zeros(1, n);
    for i = 1:n
        samples(i) = signal();
    end
end
