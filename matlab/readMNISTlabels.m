function labels = readMNISTlabels(path, n)

ain=fopen(path,'rb');

fread(ain, 2, 'int', 'ieee-be');

labels = fread(ain, n, 'uint8', 'ieee-be');

fclose(ain);

end
