function o = testDrift()

    x = 0;
    y = zeros(1, 2000);
    d = y;

    for i = 1:length(y)
        y(i) = x;
        
        X = (x + 1) / 2; % weight probability
        Y = 0.5; % increment probability
        S = (X + Y) / 2; % stochastic sum
        
        e = floor(binornd(16383, S) / 64);
        e = (e - 128) * 2 + 128 + randi([0 1]);%mod(e,2);%;- sign(e-128);
        
        e = 2 * (e / 255) - 1; % estimated weight
        x = max(-1, min(1, e)); % clamp
        
        d(i) = 2 * (binornd(16384, Y) / 16384) - 1; % estimated delta (for debugging)
    end

    plot(y, 'DisplayName', 'bob');
    ylim([-1 1]);
    
    hold on;
    plot(d);
    hold off;
    
    hl = legend('Registrador', 'Incremento');

    set(hl, 'FontSize', 12);
    set(gca,'YGrid','on');
    xlabel('(d)', 'FontSize', 16, 'Interpreter', 'latex');
end