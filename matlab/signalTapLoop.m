function signalTapLoop(stp, fn)
    cleanUp = onCleanup(@() alt_signaltap_run('END_CONNECTION'));
    
    while 1
        x = alt_signaltap_run(stp, 'unsigned', 'auto_perf');
        fn(double(x));
        drawnow();
    end
end
