function o = adds(X, Y)
    S = stochastic.genbi(0);
    o = (X .* S) + (1 - S) .* Y;
end