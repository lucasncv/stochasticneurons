function Z = stoc2d(X, Y)

    Z = arrayfun(@(x, y) bibin(ssum3(bistc(-x), bistc(-y), bistc(1))), X, Y);

end

function X = bistc(p)
    X = stc((p + 1) / 2);
end

function X = stc(p)
    X = round(rand(1, 512) + (p - 0.5));
end

function p = bibin(X)
    p = 2 * bin(X) - 1;
end

function p = bin(X)
    p = sum(X) / size(X, 2);
end

function Y = ssum2(A, B)
    Y = 0.5 * ((-1).^(1:512) + 1);
    Y = (Y & (A | B)) | (~Y & (A & B));
end

function Y = ssum3(A, B, C)
    Y = (C & (A | B)) | (~C & (A & B));
end

function Y = stanh(X, N)
    Y = counter(X, N) >= N / 2;
end

function Y = counter(X, N)
    Y = 2 * X - 1;
    c = 0;
    
    for i = 1:size(Y, 2)
        c = min(N - 1, max(0, c + Y(i)));
        Y(i) = c;
    end
end
