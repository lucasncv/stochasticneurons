function loadTrainingSet(X, T)

%% Save files
[n, m] = size(X);
x = 255 * (X + 1) / 2;

ain=fopen('inputs.mif','w');
fprintf(ain,'DEPTH = %d;\n', m * n + 1);
fprintf(ain,'WIDTH = 8;\nADDRESS_RADIX = HEX;\nDATA_RADIX = HEX;\n');
fprintf(ain,'CONTENT\nBEGIN\n');

fprintf(ain,'00000000 : %s;\n', dec2hex(n, 2));

for i=1:n
    fprintf(ain,'%s :',dec2hex((i - 1) * m + 1, 8));
    for j=1:m
        fprintf(ain,' %s',dec2hex(uint8(x(i, j)), 2));
    end
    fprintf(ain,';\n');
end

fprintf(ain,'END;\n');
fclose(ain);

[n, m] = size(T);
t = 255 * (T + 1) / 2;

ain=fopen('outputs.mif','w');
fprintf(ain,'DEPTH = %d;\n', m * n);
fprintf(ain,'WIDTH = 8;\nADDRESS_RADIX = HEX;\nDATA_RADIX = HEX;\n');
fprintf(ain,'CONTENT\nBEGIN\n');

for i=1:n
    fprintf(ain,'%s :',dec2hex((i - 1) * m, 8));
    for j=1:m
        fprintf(ain,' %s',dec2hex(uint8(t(i, j)), 2));
    end
    fprintf(ain,';\n');
end

fprintf(ain,'END;\n');
fclose(ain);

%% Run TCL script
[~,~] = system('quartus_stp_tcl -t loadsets.tcl');

end
