function [net, sim] = stochasticnet(hiddenSizes, x, t)

    net = feedforwardnet(hiddenSizes);
    net.divideFcn = '';
    
    net = train(net, x, t);
    
    inputSize = 1;
    outputSize = 1;
    
    w = zeros(hiddenSizes(1), inputSize + 1);
    w(:,1) = net.IW{1};
    w(:,2) = net.b{1};
    
    w = adapt(w);
    
    w2 = zeros(outputSize, hiddenSizes(1) + 1);
    w2(1,:) = [net.LW{2,1} net.b{2}];
    
    w2 = adapt(w2);
    
    % -- Setup the network
    layerSizes = [1 hiddenSizes 1];
    layerCount = length(layerSizes);
    layers = struct('size', num2cell(layerSizes));
    
    for i = 2:layerCount
        layers(i).synapseCount = layers(i-1).size + 1;
        layers(i).potential = zeros(layers(i).size, 1);
        layers(i).activation = zeros(layers(i).size, 1);
        layers(i).localError = zeros(layers(i).size, 1);
        layers(i).weights = 2 * rand(layers(i).size, layers(i).synapseCount) - 1;
    end
    
    layers(2).weights = w;
    layers(3).weights = w2;
    
    % -- Feedforward
    function outputs = simulate(inputs)
        
        layers(1).activation = inputs';

        for l = 2:layerCount

            % Calculate action potential
            layers(l).potential = layers(l).weights * [layers(l - 1).activation; 1];
            layers(l).potential = layers(l).potential / layers(l).synapseCount;
            
            % Calculate activation
            layers(l).activation = activate(layers(l).potential);
            
        end
       
        outputs = layers(layerCount).activation';
        
    end

    sim = @(i) simulate(i);
    
end

function w = adapt(w)
    disp(w);
    
    w = w * size(w, 2);
    w = w / 6;
    
    disp(w);
    
    g = max(abs(w), [], 2);
    g(:,2) = 1;
    g = max(g, [], 2);
    
    w = diag(1 ./ g) * w;
end

function o = activate(u)
    o = tansig(u * 6);
end
