function plotneterrors(sim, X, T)
    
    errors = zeros(1, size(X, 1));

    for i = 1:size(X, 1)
        o = sim(X(i,:));
        errors(i) = sum((T(i,:) - o) .^ 2) / size(T, 2);
    end
    
    plot(errors);
    
    xlabel('Amostras', 'Interpreter', 'latex', 'FontSize', 14);
    ylabel('Erro Quadr\''atico', 'Interpreter', 'latex', 'FontSize', 14);
end
