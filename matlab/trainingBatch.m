function [stats] = trainingBatch(hiddenSizes, x, t, varargin)

    % -- Check parameters
    p = inputParser;
    addParameter(p, 'ExecutionCount', 20, @isnumeric);
    addParameter(p, 'Classification', 'off');
    addParameter(p, 'HiddenLayers', 1);
    
    parse(p, varargin{:});
    parameters = p.Results;
    
    % -- Batch
    stat = zeros(1, 2);
    stats = zeros(length(hiddenSizes), parameters.ExecutionCount);
    statType = 1;
    
    if strcmp(parameters.Classification, 'on')
        statType = 2;
    end

    for n = 1:length(hiddenSizes)
        layerSizes = [size(x, 2) repmat(hiddenSizes(n), 1, parameters.HiddenLayers) size(t, 2)];
        
        for e = 1:parameters.ExecutionCount
            [~,~,stat(1),stat(2)] = stochasticTraining(layerSizes, x, t, 'PlotPerformance', 'off', 'CalculateRatio', parameters.Classification);
            stats(n, e) = stat(statType);
        end
        
        disp('OK');
    end
    
    if strcmp(parameters.Classification, 'on')
        plotClassificationBatch(stats, hiddenSizes);
    else
        plotPerformanceBatch(stats, hiddenSizes);  
    end
    
end
