function plot2Dnet(sim)

    r = -1:0.1:1;
    z = zeros(length(r));
    
    for x = 1:length(r)
        for y = 1:length(r)
            o = sim([r(x) r(y)]);
            z(y, x) = o;
        end
    end
    
    surf(r, r, z);
    zlim([-1 1]);
    
    xlabel('$x_1$', 'Interpreter', 'latex', 'FontSize', 14);
    ylabel('$x_2$', 'Interpreter', 'latex', 'FontSize', 14);
    zlabel('$y$', 'Interpreter', 'latex', 'FontSize', 14);
    
end
