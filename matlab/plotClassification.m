function ratio = plotClassification(X, T)
    stp = 'D:/Dev/FPGA/StochasticNeurons/Top.stp';
    
    Z = alt_signaltap_run(stp, 'unsigned', 'auto_testing');
    alt_signaltap_run('END_CONNECTION');
    
    Z = Z(130:129+size(X,1),:);
    Z = double(Z) / 128 - 1;
    
    iX = Z(:,1:size(X, 2));
    iT = Z(:,size(X, 2)+1:size(Z,2));
    
    [~,iT] = max(iT, [], 2);
    [~,T] = max(T, [], 2);
    
    ratio = sum(iT == T) / size(X,1);
    
    %scatter3(X(:,1), X(:,2), X(:,3));
end
