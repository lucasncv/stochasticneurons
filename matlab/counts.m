function C = counts(X, N)
    C = continuous(X, N);
end

function C = single(X, N)
    C = zeros(1, N);
    c = 0;
    
    for i = 1:N
        C(i) = c + X(i);
        c = C(i);
    end
end

function C = progressive(X, N)
    C = single(X, N);
    e = 0;
    p = 16;

    for i = 1:N
        if i == p
            e = round(C(i) / 4);
            e = 2 * (e / (p / 4)) - 1;
            p = p * 2;
        end
        C(i) = e;
    end
end

function C = continuous(X, N)
    n = length(X);
    C = zeros(1, n);
    c = 0;
    
    for i = 1:n
        if i >= N && mod(i - N, 128) == 0
            c = round(c / 2);
        end
        C(i) = c + X(i);
        c = C(i);
    end
    
    C(1:(N/2)) = C(1:(N/2)) * 2;
    C = 2 * (C / N) - 1;
end
