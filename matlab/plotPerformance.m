function plotPerformance(n)
    stp = 'D:/Dev/FPGA/StochasticNeurons/Top.stp';
    
    X = alt_signaltap_run(stp, 'unsigned', 'auto_training');
    alt_signaltap_run('END_CONNECTION');
    
    X = X(513:4096);
    X = double(X) / 128 - 1;
    
    e = floor(3584 / n); % epochs
    N = e * n;
    
    X = reshape(X(1:N), [n e]);
    X = sum(X .* X) / n;
    
    plot(1:e, X, '-.', 'DisplayName', 'Desempenho');
    
    ylim([0 1]);
    xlim([1 e]);
    
    xlabel('\''Epocas', 'Interpreter', 'latex', 'FontSize', 14);
    grid on;
end
