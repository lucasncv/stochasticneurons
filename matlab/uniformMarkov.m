function X = uniformMarkov(n)

X = stochastic.genbi(0);
c = 0;

for i = 1:size(X, 1)
    if (X(i))
        c = c + 1;
    else
        c = c - 1;
    end
    
    if (c >= n)
        c = c - n;
    elseif (c < 0)
        c = c + n;
    end
    
    X(i) = c;
end

end