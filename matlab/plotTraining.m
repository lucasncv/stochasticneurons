function plotTraining(setSize, outputCount)
    stp = 'D:/Dev/FPGA/StochasticNeurons/Top.stp';
    
    X = alt_signaltap_run(stp, 'unsigned', 'auto_training');
    alt_signaltap_run('END_CONNECTION');
    
    X = X(513:4096, 1:outputCount);
    X = double(X) / 128 - 1;
    
    x = 1:500;
    E = zeros(length(x), 1); % epochs
    
    for e = x
        j = (e - 1) * setSize;
        
        %E(e,1:3) = X(j+1,1:3);
        
        for i = 1:setSize
            p = X(j+i, :) .^ 2;
            E(e) = E(e) + sum(p);
        end
    end
    
    E = E / setSize;
    
    plot(x, E, '-', 'DisplayName', 'Desempenho');
    hold on;

    %for i = 1:3
    %    plot(x, E(:,i), 'DisplayName', ['$w_', num2str(i), '$']);
    %end
    
    hold off;
    
    ylim([0 1]);
    xlim([1 length(x)]);
    
    xlabel('\''Epocas', 'Interpreter', 'latex', 'FontSize', 14);
    grid on;
end
