
n = 8;
s = 0;

T = zeros(8,256); 

for i = 1:size(T, 2)
    for j = 1:256
        
        s = mod(s + (randi(2) - 1) * 2 - 1, 8);
        T(s+1,i:size(T,2)) = T(s+1,i:size(T,2)) + 1;
        
    end
end

for i = 1:size(T, 2)
    T(:,i) = T(:,i) / (256 * i);
end

figure;
hold on;

for i = 1:size(T,1)
    plot(1:256:65536, T(i,:));
end

hold off;
ylim([0 0.5]);
xlim([0 65536]);