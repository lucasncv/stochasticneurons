function plotPerformanceBatch(perfs, hiddenSizes)
    avg = mean(perfs, 2);
    v = var(perfs, [], 2);
    
    %upper = max(perfs, [], 2) - avg;
    %lower = avg - min(perfs, [], 2);
    
    errorbar(hiddenSizes, avg, v, 'LineWidth', 1);
    ylim([0 0.5]);
    
    xlabel('Neur\^onios em cada Camada Oculta', 'Interpreter', 'latex', 'FontSize', 14);
    ylabel('Desempenho M\''edio', 'Interpreter', 'latex', 'FontSize', 14);
end
