function o = nntest(type, varargin)
    if strcmp('ff', type)
        o = feedforward(varargin{1}, varargin{2});
    else
        o = elmann(varargin{1});
    end
end

function o = feedforward(a, b)
    A = stochastic.genbi(a);
    B = stochastic.genbi(b);
    
    A2 = stochastic.neuron({A, B}, {1, 1}, 1);
    B2 = stochastic.neuron({A, B}, {-1, -1}, 1);
    
    X = stochastic.neuron({A2, B2}, {1, 1}, -1);
    o = stochastic.binbi(X);
end

function o = elmann(x)
    Ar = stochastic.genbi(-1);
    Br = stochastic.genbi(-1);
    Cr = stochastic.genbi(-1);
    
    o = zeros(1, length(x));
    
    for i = 1:length(x)
        X = stochastic.genbi(x(i));

        nAr = stochastic.neuron({X, Ar, Br, Cr}, {1, 0, 0, 1}, 1);
        nBr = stochastic.neuron({X, Ar, Br, Cr}, {-1, 0, 0, -1}, 1);
        nCr = stochastic.neuron({X, Ar, Br, Cr}, {1, 0, 0, 0}, 0);
        
        Ar = nAr; % OR
        Br = nBr; % NAND
        Cr = nCr; % last value
        
        X = stochastic.neuron({Ar, Br, Cr}, {1, 1, 0}, -0.65);
        o(i) = stochastic.binbi(X);
    end
end

