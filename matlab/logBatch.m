function logBatch(ticks)
    ax = gca;
    ax.XTick = ticks;
    grid on;
    ax.XScale = 'log';
    ax.XMinorTick = 'off';
    ax.XMinorGrid = 'off';
end