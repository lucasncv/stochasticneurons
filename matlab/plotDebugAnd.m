function plotDebugAnd(X)
    X = double(X) / 127.5 - 1;
    
    x = 1:896;
    
    plot(x, X(:,1), 'DisplayName', 'Performance');
    hold on;
    
    plot(x, X(:,2), 'DisplayName', 'Weight 3');
    plot(x, X(:,3), 'DisplayName', 'Weight 4');
    
    plot(x, X(:,4), '--', 'DisplayName', 'Weight 7');
    
    %plot(x, X(:,5), 'ro', 'DisplayName', 'Output Neuron Local Eror');
    plot(x, X(:,6), 'r-', 'DisplayName', 'Hidden Neuron Local Eror');
    
    %plot(x, X(:,7), 'go', 'DisplayName', 'Output Neuron Derivative');
    %plot(x, X(:,8), 'g*', 'DisplayName', 'Hidden Neuron Derivative');

    %plot(x, X(:,9), 'bo', 'DisplayName', 'Output Neuron Net Input');
    %plot(x, X(:,10), 'b*', 'DisplayName', 'Hidden Neuron Net Input');
    
    %plot(x, X(:,11), ':', 'DisplayName', 'Hidden Neuron Net Error');
    
    plot(x, X(:,11), '-', 'DisplayName', 'Delta Weight 3');
    plot(x, X(:,12), '-', 'DisplayName', 'Half Weight 3');
    plot(x, X(:,13), '-', 'DisplayName', 'Estimated Weight 3');
    plot(x, X(:,14), ':', 'DisplayName', 'Learning Factor');
    
    hold off;
    ylim([-1 1]);
    grid on;
end
