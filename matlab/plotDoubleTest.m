function plotDoubleTest()
    stp = 'D:/Dev/FPGA/StochasticNeurons/Top.stp';
    
    X = alt_signaltap_run(stp, 'unsigned', 'test_double');
    alt_signaltap_run('END_CONNECTION');
    
    X = X(129:1024,:);
    X = double(X) / 128 - 1;
    
    x = 1:896;
    
    plot(x, X(:,1), '-', 'DisplayName', 'Delta');
    hold on;
    plot(x, X(:,2), '-', 'DisplayName', 'Accumulator');
    
    Y = X(:,2);
    
    for i = 2:896
        Y(i) = min(1, max(-1, Y(i-1) + X(i,1)));
    end
    
    %plot(x, Y, 'o', 'DisplayName', 'Simulated Accumulator');
    
    hold off;
    ylim([-1 1]);
    grid on;
end
