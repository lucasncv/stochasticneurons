function number = stochastic2Binary(bits)
    number = sum(bits) / size(bits, 2);
end
