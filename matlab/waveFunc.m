function z = waveFunc(x, y)
    z = (4 * x) .* exp(-4 * (x .^ 2 + y .^ 2));
end
