function z = gaussian(x, y)
    A = 1;
    sig = 0.2;
    
    dx = -0.4 * (x .^ 2);
    dy = -0.4 * (y .^ 2);
    
    z = A * exp(dx + dy);
    
    %f(x,y) = exp(-x^2
end
