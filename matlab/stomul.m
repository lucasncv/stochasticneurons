function o = stomul(a, b)
o = (a * b / 128) - a - b + 256;
end
