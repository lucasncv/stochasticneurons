function o = handlePerfHistory(data)
    o = double(data(129:1024));
    o = reshape(o, 4, 224);
    o = 2 * (o / 255) - 1;
    o = 2 .* (o .^ 2);
    o = sum(o);
    
    plot(o);
end
