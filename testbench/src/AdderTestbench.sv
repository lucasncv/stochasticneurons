module AdderTestbench();

logic CLOCK = 0;
always #5 CLOCK = !CLOCK;

bit [17:0] SW;
wire [17:0] LEDR;
wire [8:0] LEDG;

// -- Linear Feedback Shift Register.
bit [31:0] RandomStreams;
LFSR32 lfsr(CLOCK, RandomStreams);

// -- SNG.
wire [3:0] Streams;

BitStreamGenerator #(4) gen0(CLOCK, 4'b0000, RandomStreams[3:0], Streams[0]);
BitStreamGenerator #(4) gen1(CLOCK, 4'b1000, RandomStreams[7:4], Streams[1]);
BitStreamGenerator #(4) gen2(CLOCK, 4'b1111, RandomStreams[11:8], Streams[2]);
BitStreamGenerator #(4) gen3(CLOCK, 4'b1000, RandomStreams[15:12], Streams[3]);

logic Output;

StochasticGenericAdder #(3) chain(CLOCK, Streams[2:0], Output);

wire AccumulatorReady10;
bit [10:0] Probability10;
BitStreamAccumulator #(10) acc10(RESET, CLOCK, Output, Probability10, AccumulatorReady10);

// -- Testing.
//integer fp;

//initial begin
//	#10
//
//	fp = $fopen("D:\\Dev\\prob.dat", "w");
//	repeat (256) #40960 $fdisplay(fp, Probability[11:8]);
//	$fclose(fp);
//
//	$stop;
//end

endmodule
