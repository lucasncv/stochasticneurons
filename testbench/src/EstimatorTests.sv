/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Top Module
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module EstimatorTests();

logic [4:0] InputProbability = 5'b0;

// -- Control.
logic RESET = 0;
logic CLOCK = 0;
always #5 CLOCK = !CLOCK;

// -- Linear Feedback Shift Register.
bit [31:0] RandomStreams;
LFSR32 lfsr(CLOCK, RandomStreams);

// -- Generators.
wire InputStream;
BitStreamGenerator #(4) gen (CLOCK, InputProbability, RandomStreams[31:28], InputStream);

// -- Accumulators.
logic [7:0] EstimatedProbability;
logic AccReady;

ProbabilityEstimator #(8) est (RESET, CLOCK, InputStream, EstimatedProbability, AccReady);

// -- Testing.
integer fp;
integer n = 0;

initial begin
	fp = $fopen("D:\\prob.dat", "w");
end

task Sample ();
		$fdisplay(fp, EstimatedProbability);

		if (InputProbability[4]) begin
			$fclose(fp);
			$stop;
		end
		
		InputProbability = InputProbability + 1;
endtask

always @(posedge CLOCK)
	if (AccReady) begin
		Sample();
	end

endmodule
