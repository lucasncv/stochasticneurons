module BitStreamGeneration();

logic CLOCK = 0;
always #5 CLOCK = !CLOCK;

bit [17:0] SW;
wire [17:0] LEDR;
wire [8:0] LEDG;

bit [31:0] RandomStreams;
LFSR32 lfsr(CLOCK, RandomStreams);

logic [8:0] Probability;
BitStreamAccumulator #(8) acc(1'b0, CLOCK, 1'b1, Probability, LEDG[0]);

// -- Testing.
//integer fp;

//initial begin
//	#10
//
//	fp = $fopen("D:\\Dev\\prob.dat", "w");
//	repeat (256) #40960 $fdisplay(fp, Probability[11:8]);
//	$fclose(fp);
//
//	$stop;
//end

endmodule
