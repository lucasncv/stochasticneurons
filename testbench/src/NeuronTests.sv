/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Top Module
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module NeuronTests();

logic [1:0] NeuronInputs = 2'b0;
logic [3:0][1:0] InputSets = { 2'b00, 2'b01, 2'b10, 2'b11 };
logic [3:0] OutputSets = 4'b1110;

// -- Control.
logic RESET = 0;
logic CLOCK = 0;
always #5 CLOCK = !CLOCK;

// -- Linear Feedback Shift Register.
bit [31:0] RandomStreams;
LFSR32 lfsr(CLOCK, RandomStreams);

// -- Address generators.
bit [2:0] AddressBitStreams;
AddressBitStreamsGenerator addr(CLOCK, RandomStreams[17:0], AddressBitStreams);

// -- Address delays.
wire [2:0] LUTADDR [1:3];
SignalDelays #(3, 3) delays(CLOCK, AddressBitStreams, LUTADDR);

// -- Synapses.
wire [3:1] SynapseOutputs;
wire NeuronOutput, NeuronDerivative, Gradient;

logic ExpectedOutput;
logic AdjustedWeights;
logic Learning = 0;
logic LearningFinished = 1'b1;

AdaptableSynapse syn1 (RESET, CLOCK, Learning,, 8'd51, NeuronInputs[0], SynapseOutputs[1], Gradient,, LUTADDR[1]);
AdaptableSynapse syn2 (RESET, CLOCK, Learning,, 8'd208, NeuronInputs[1], SynapseOutputs[2], Gradient,, LUTADDR[2]);
AdaptableSynapse syn3 (RESET, CLOCK, Learning, AdjustedWeights, 8'd145, 1'b1, SynapseOutputs[3], Gradient,, LUTADDR[3]);

Neuron #(3) neu1 (CLOCK, SynapseOutputs, NeuronOutput, NeuronDerivative);
OutputLayerTrainer tra1 (CLOCK, NeuronDerivative, NeuronOutput, ExpectedOutput, Gradient);

// -- Accumulators.
logic [7:0] EstimatedProbability;
logic AccReady;

ProbabilityEstimator #(8) est (RESET | ~LearningFinished, CLOCK, NeuronOutput, EstimatedProbability, AccReady);

// -- Testing.
integer fp;

integer epoch = 0;
integer set = 0;
integer n = 0;

initial begin
    RESET = 1'b1;
    #40 RESET = 1'b0;
    
    fp = $fopen("D:\\prob.dat", "w");
    
    //KeepLearning();
end

task KeepLearning ();
    
    NeuronInputs = InputSets[set];
    ExpectedOutput = OutputSets[set];
    
    #100 Learning = 1'b1;
    
    set = set + 1;
    if (set >= 4) begin
        set = 0;
        epoch = epoch + 1;
        $display("Entering Epoch %d", epoch);
    end
    
endtask

always @(posedge CLOCK)
    if (AdjustedWeights) begin
        Learning = 1'b0;
        NeuronInputs = 2'b0;
        
        if (epoch < 5)
            KeepLearning();
        else
            LearningFinished = 1'b1;
    end

task Sample ();
		$fdisplay(fp, EstimatedProbability);
        n = n + 1;
        
        case (n)
            0: NeuronInputs = 2'd0;
            1: NeuronInputs = 2'd1;
            2: NeuronInputs = 2'd2;
            3: NeuronInputs = 2'd3;
            4: NeuronInputs = 2'd0;
            5: NeuronInputs = 2'd1;
            6: NeuronInputs = 2'd2;
            7: NeuronInputs = 2'd3;
        endcase
        
        if (n == 4) begin
            LearningFinished = 1'b0;
            KeepLearning();
        end else if (n >= 8) begin
			$fclose(fp);
			$stop;
		end
endtask

always @(posedge CLOCK)
	if (AccReady) begin
		Sample();
	end

endmodule
