/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Top Module
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module NetworkTests();

logic CLOCK = 1'b0;

NetworkPath #(3) paths ();

NetworkLayer #(3) layer (.iClock(CLOCK), .iRandomSource(CLOCK), .ioPaths(paths.neuron));

endmodule

