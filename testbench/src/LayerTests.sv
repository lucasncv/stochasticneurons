module LayerTests();

// -- Control.
logic RESET = 0;
logic CLOCK = 0;
always #5 CLOCK = !CLOCK;

// -- Control.
wire iClock = CLOCK;
wire iReset = RESET;
logic iLearn = '0;

// -- Linear Feedback Shift Register.
bit [31:0] RandomStreams;
LFSR32 lfsr(iClock, RandomStreams);

// -- Address generators.
bit [2:0] AddressBitStreams;
AddressBitStreamsGenerator addr(iClock, RandomStreams[17:0], AddressBitStreams);

wire [7:0] LEDG;

//
//TrainingControl #(.I(2), .O(1)) training
//(
//	.iClock,
//	.iReset,
//	.iNext(iLearn),
//	.iGenerationSeed(AddressBitStreams),
//	.oReady(LEDG[0]),
//	.oInputSet('{ LEDG[7], LEDG[6] }),
//	.oOutputSet('{ LEDG[5] })
//);


localparam WEIGHT_COUNT = 3;

wire Weights [WEIGHT_COUNT];
wire DeltaWeights [WEIGHT_COUNT];

wire [1:0] Address;
tri [7:0] DataA;
tri [7:0] DataB;

logic CopyDirection = '0;
logic Copying = '0;
logic DoneCopying;

logic [7:0] Values [3] = '{ 8'd42, 8'd120, 8'd254 };

assign DataA = Values[Address];

IByteMemory #(.N(WEIGHT_COUNT)) mem();


WeightBank #(.N(WEIGHT_COUNT)) weightssss
(
	.iClock,
	.iLearn,
	.iRandomSource(RandomStreams[22]),
	.iGenerationSeed(AddressBitStreams),
	.iDeltaWeights(DeltaWeights),
	.oWeights(Weights),
	.oLearned(),
	.DirectAccess(mem)
);
//
//MemoryCopier #(.WIDTH(8), .N(WEIGHT_COUNT)) copier
//(
//	.iClock,
//	.iReset(!Copying),
//	.iDirection(CopyDirection),
//	.oAddress(Address),
//	.ioDataA(DataA),
//	.ioDataB(DataB),
//	.oDone(DoneCopying)
//);

always @(posedge iClock)
	if (DoneCopying)
		Copying <= '0;

initial #10 begin

	iLearn = '1;
	
end

endmodule
