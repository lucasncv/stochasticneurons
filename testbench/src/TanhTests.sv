/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Top Module
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module TanhTests();

logic [3:0] InputProbability = 4'b0;

// -- Control.
logic RESET = 0;
logic CLOCK = 0;
always #5 CLOCK = !CLOCK;

// -- Linear Feedback Shift Register.
bit [31:0] RandomStreams;
LFSR32 lfsr(CLOCK, RandomStreams);

// -- Generators.
wire InputStream;
BitStreamGenerator #(4) gen (CLOCK, InputProbability, RandomStreams[31:28], InputStream);

// -- Tanh.
bit Primitive, Derivative;
StochasticTanh tanh (CLOCK, InputStream, Primitive, Derivative);

// -- Accumulators.
logic [3:0] RoundedProbability;
logic AccReady;

RoundedAccumulator #(8,4) acc (RESET, CLOCK, Derivative, RoundedProbability, AccReady);

// -- Testing.
integer fp;
integer n = 0;

initial begin
	fp = $fopen("D:\\prob.dat", "w");
end

task Sample ();
		$fdisplay(fp, RoundedProbability);
		n = n + 1;

		if (n >= 32) begin

			if (InputProbability == 4'hF) begin
				$fclose(fp);
				$stop;
			end
			
			n = 0;
			InputProbability = InputProbability + 1;

		end
endtask

always @(posedge CLOCK)
	if (AccReady) begin
	#5000 Sample();
end

endmodule

module RoundedAccumulator(RESET, CLOCK, INPUT, OUTPUT, READY);

parameter ACCWIDTH = 8;
parameter WIDTH = 4;

input wire RESET;
input wire CLOCK;
input wire INPUT;
output logic [WIDTH-1:0] OUTPUT;
output wire READY;

wire [ACCWIDTH:0] AccumulatorOutput;

BitStreamAccumulator #(ACCWIDTH) acc (RESET, CLOCK, INPUT, AccumulatorOutput, READY);

wire [ACCWIDTH:0] RoundedFullRes = AccumulatorOutput + { 1'b1, { (ACCWIDTH - WIDTH - 1) { 1'b0 } } };

always_ff @(posedge CLOCK)
	OUTPUT <= RoundedFullRes[ACCWIDTH] ? { WIDTH { 1'b1 } } : RoundedFullRes[ACCWIDTH-WIDTH +: WIDTH];

endmodule

