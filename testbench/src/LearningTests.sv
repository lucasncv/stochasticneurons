/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Top Module
* Author: Lucas Neves Carvalho
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
module LearningTests();

// -- Control.
logic RESET = 0;
logic CLOCK = 0;
always #5 CLOCK = !CLOCK;
logic LEARN = 0;

logic [3:0] OutputSets = 4'b0111;
logic [1:0] NeuronInputs = 2'b0;

TopLearningXOR top (CLOCK, ~{ 2'b0, LEARN, RESET }, { 3'b0, OutputSets, 9'b0, NeuronInputs },,);

// -- Testing.
integer fp;

integer epoch = 0;
integer set = 0;
integer n = 0;

initial begin
    RESET = 1'b1;
    #40 RESET = 1'b0;
    
    //fp = $fopen("D:\\prob.dat", "w");

	LEARN = 1'b1;
end

endmodule
